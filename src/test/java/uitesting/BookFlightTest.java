//Adapted from demo from https://guides.gradle.org/building-java-web-applications/#unit_test_the_servlet_using_mockito

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
/*
import org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
*/
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class BookFlightTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }
	@Test
    public void bookFlight() throws Exception {   
		//Books and pays for a flight for test@test.com
		WebDriverWait wait = new WebDriverWait(driver, 10);
        // driver.get(root+"search");
		driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("ATL");
		driver.findElement(By.id("arrivalAirport")).sendKeys("GIG");
		driver.findElement(By.id("departureDate")).sendKeys("24122017");
		driver.findElement(By.id("searchInfo")).submit();
		//Waits until "Mon, Dec 25" is found somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.linkText("View"),"View"));
		driver.findElement(By.linkText("View")).click();
		//Goes to view flight details page
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Book")));
		driver.findElement(By.linkText("Book")).click();
		//Books the flight ticket for test@test.com
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bookseat")));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("bookseat")).click();
		//Clicking on bookseat sends to bookings page - proceed to payment
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Proceed to Payment")));
		driver.findElement(By.linkText("Proceed to Payment")).click();
		//Enters payment details
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Payment"));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("nameOnCard")).sendKeys("Test McTest");
		driver.findElement(By.id("creditCardNumber")).sendKeys("3234");
		driver.findElement(By.id("confirm")).click();

    }
}