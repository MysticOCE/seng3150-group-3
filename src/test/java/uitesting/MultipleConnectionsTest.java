//Makes multiple connections to the database by constantly refreshing the page.

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
/*
import org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
*/
import org.openqa.selenium.chrome.ChromeDriver;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class MultipleConnectionsTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    @Test
    public void purchaseHistoryMultipleConnect() throws Exception {   
		
		//	Commented out - If not fixed, these sections will require restarting the server!
		//Constantly refreshes the search results page.
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"purchase-history?email=test%40test.com");
		for(int i = 0; i < 50; i++){
			driver.navigate().refresh();
			//Waits until "Results" is found in header 1
			wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Purchase History"));
		}
		//Goes to search. Fails if search results in a 500 error.
        driver.get(root+"search");
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Search"));
		
    }
    @Test
    public void resultsMultipleConnect() throws Exception {   
		//Constantly refreshes the search results page.
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"results?flightType=oneway&departureAirport=ATL&arrivalAirport=GIG&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		for(int i = 0; i < 50; i++){
			driver.navigate().refresh();
			//Waits until "Results" is found in header 1
			wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Results"));
		}
		//Goes to search. Fails if search results in a "HTTP Status 500 – Internal Server Error" error.
        driver.get(root+"search");
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Search"));
    }
}