//Checks the navigation between pages of the site via button presses.

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class NavigationTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }
	@Test
	public void allPagesHeaderNavigation() throws Exception{
		//Tests the header navigation from all pages.
		headerNavigation(root+"search");
		headerNavigation(root+"purchase-history");
		headerNavigation(root+"bookings");
		headerNavigation(root+"login");
		headerNavigation(root+"results?flightType=oneway&departureAirport=ATL&arrivalAirport=GIG&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
	}
    
    public void headerNavigation(String url) throws Exception {      
        driver.get(url);
		//Checks search, past flights, bookings and logout links
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.findElement(By.linkText("Search")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Search"));
		//Goes to purchase history
		driver.findElement(By.linkText("Past Flights")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Purchase History"));
        driver.get(url);
		//Goes to Bookings
		driver.findElement(By.linkText("Bookings")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
        driver.get(url);
		//Goes to Logout
		driver.findElement(By.linkText("Logout")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Login"));
        //driver.get("http://localhost:8080/FlightPub-0.2/search");
    }
	
	@Test
	public void flightDetailsSingleLegNavigation() throws Exception {      
        //Checks navigation from a flight to a booking for the flight and its reviews. Single leg flight
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get(root+"flight-details?id=10565");
		//Back to bookings
		driver.findElement(By.linkText("Back")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
		driver.get(root+"flight-details?id=10565");
		//Book Ticket for the flight.
		driver.findElement(By.linkText("Book")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Atlanta (ATL)"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Miami (MIA)"));
		driver.get(root+"flight-details?id=10565");
		//Reviews for flight
		driver.findElement(By.linkText("Reviews")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Reviews"));
	}
	@Test
	public void flightDetailsMultiLegNavigation() throws Exception {      
        //Checks navigation from flight details when it shows a multi leg flight
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get(root+"flight-details?id=10565,30564");
		//Back to bookings
		driver.findElement(By.linkText("Back")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
		driver.get(root+"flight-details?id=10565,30564");
		//Book Ticket for the flight.
		driver.findElement(By.linkText("Book")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Atlanta (ATL)"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Miami (MIA)"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Rio De Janeiro (GIG)"));
		//Reviews for flight
		driver.get(root+"flight-details?id=10565,30564");
		driver.findElement(By.linkText("Reviews")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Reviews"));
	}
	
	@Test
	public void bookTicketNavigation() throws Exception{
		//Navigation from book ticket
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get(root+"book-ticket?id=30582");
		//To bookseat
		driver.findElement(By.id("bookseat")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
		driver.get(root+"book-ticket?id=30582");
		//bookings without booking the seat
		driver.findElement(By.linkText("Return to bookings")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
	}
	
	@Test
	public void bookingsNavigation() throws Exception{
		//Test buttons on page bookings.
		//Assumes test@test.com has no flights booked.
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"bookings?email=test%40test.com");
		bookFlightBookings();	//Books flight to test view and delete buttons. 
		driver.get(root+"bookings?email=test%40test.com");
        //Add flights
		driver.findElement(By.linkText("Add Flight")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Search"));
        driver.get(root+"bookings?email=test%40test.com");
		//Proceed to payment
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Proceed to Payment")));
		driver.findElement(By.linkText("Proceed to Payment")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Payment"));
        driver.get(root+"bookings?email=test%40test.com");
		//View
		driver.findElement(By.linkText("View")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h2"),"Flight Details"));
        driver.get(root+"bookings?email=test%40test.com");
		//Delete
		driver.findElement(By.linkText("Delete")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Bookings"));
	}
	
	public void bookFlightBookings() throws Exception{
		//Books a flight for Test@test.com
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("ATL");
		driver.findElement(By.id("arrivalAirport")).sendKeys("GIG");
		driver.findElement(By.id("departureDate")).sendKeys("24122017");
		driver.findElement(By.id("searchInfo")).submit();
		//Waits until "Mon, Dec 25" is found somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.linkText("View"),"View"));
		driver.findElement(By.linkText("View")).click();
		//Goes to view flight details page
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Book")));
		driver.findElement(By.linkText("Book")).click();
		//Books the flight ticket for test@test.com
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bookseat")));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("bookseat")).click();
	}
	
	@Test
	public void pastFlightNavigation() throws Exception{
		//Tests navigation for past flights.
		//Assumes test@test.com has purchased flights
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"purchase-history?email=test%40test.com");
		//View flight
		driver.findElement(By.linkText("View")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h2"),"Flight Details"));
        driver.get(root+"purchase-history?email=test%40test.com");
		//Create review
		driver.findElement(By.linkText("Create Review")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Create Review"));
        driver.get(root+"purchase-history?email=test%40test.com");
		
		
	}
	
	@Test
	public void viewReviewsNavigation() throws Exception{
		//Tests navigation from viewReviews to flight details
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"view-reviews?flight=10564");
		//View flight
		driver.findElement(By.linkText("Back")).click();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h2"),"Flight Details"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"ATL"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"MIA"));
		
	}
}