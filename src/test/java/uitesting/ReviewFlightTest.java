//Adapted from demo from https://guides.gradle.org/building-java-web-applications/#unit_test_the_servlet_using_mockito
//Note that this means the review created by the test case will need to be removed when testing this use case again.

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
/*
import org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
*/
import org.openqa.selenium.chrome.ChromeDriver;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class ReviewFlightTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    //Test no longer succeeds due to checking for old text on page. Functionality done by ReviewTagTest tests.
    //@Test
    public void reviewFlight() throws Exception {   
		//Reviews flight directly in URL
		//Assumes test@test.com email is in the database, and has booked and purchased the flight related to the review. If not, will throw an error.
		//Assumes the comment "Test Review by test review case" is not part of a review already. If it is, this test case may suceed where it shouldn't.
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"create-review?flight=30565");
		//Searches for "Atlanta" and "Rio De Janerio" somewhere in the body.
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("comment")));
		driver.findElement(By.id("comment")).sendKeys("Test Review by test review case");
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("review")).submit();
		
		//View reviews to see if the comment was made.
        driver.get(root+"view-reviews?flight=30565");
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Reviews for Japan Airlines Flight JL1735 | FlightPub"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Test Review by test review case"));
		
    }
    //Test deprecated by updates. controllertest.ReviewTagTest now does this.
    //@Test
    public void reviewFlightInvalidEmail() throws Exception {   
		//Assumes testInvalid@test.com email is not in the database. If it is, this test case will succeed.
		
		//Reviews flight directly in URL
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get("http://localhost:8080/FlightPub-0.2/create-review?flight=30565");
		//Searches for "Atlanta" and "Rio De Janerio" somewhere in the body.
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("comment")));
		driver.findElement(By.id("comment")).sendKeys("Invalid Review by invalid test review case");
		driver.findElement(By.id("email")).sendKeys("testInvalid@test.com");
		driver.findElement(By.id("review")).submit();
		//View reviews to see if the comment was made.
        driver.get("http://localhost:8080/FlightPub-0.2/view-reviews?flight=30565");
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Reviews for Japan Airlines Flight JL1735 | FlightPub"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Invalid Review by invalid test review case"));
    }
}