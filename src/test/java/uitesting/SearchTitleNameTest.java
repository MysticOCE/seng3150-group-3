//Adapted from demo from https://guides.gradle.org/building-java-web-applications/#unit_test_the_servlet_using_mockito

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.UnableToSetCookieException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class SearchTitleNameTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    @Test
    public void getTitle() throws Exception {      
        driver.get(root+"search");
		assertEquals("Search | FlightPub",driver.getTitle());
    }
}