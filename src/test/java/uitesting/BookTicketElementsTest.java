//Checks the elements of the book ticket page.

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
/*
import org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
*/
import org.openqa.selenium.chrome.ChromeDriver;
import seng3150.group3.Utility;

import javax.rmi.CORBA.Util;

import static org.junit.Assert.assertEquals;

public class BookTicketElementsTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    @Test
    public void checkBookTicketElementsATLGIG() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"results?flightType=oneway&departureAirport=ATL&arrivalAirport=GIG&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		//Searches for "Atlanta" and "Rio De Janerio" somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Rio De Janeiro"));
		driver.findElement(By.linkText("View")).click();
		//Navigates to book-ticket and checks if Atlanta and Rio De Janerio are somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Rio De Janeiro"));
		
    }
		
    @Test
    public void checkBookTicketElementsLGAORD() throws Exception {
		//Test for LGA and ORD
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
        driver.get(root+"results?flightType=oneway&departureAirport=LGA&arrivalAirport=ORD&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		//Searches for names of cities somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"New York - Laguardia"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Chicago - OHare Intl."));
		driver.findElement(By.linkText("View")).click();
		//Navigates to book-ticket and checks if city names are somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"New York - Laguardia"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Chicago - OHare Intl."));
		
    }
    @Test
    public void checkBookTicketElementsMIALGA() throws Exception {
		//Test for Miami and New York - Laguardia
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
        driver.get(root+"results?flightType=oneway&departureAirport=MIA&arrivalAirport=LGA&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		//Searches for names of cities somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Miami"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"New York - Laguardia"));
		driver.findElement(By.linkText("View")).click();
		//Navigates to book-ticket and checks if city names are somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Miami"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"New York - Laguardia"));
		
    }
		
		
		
		
}