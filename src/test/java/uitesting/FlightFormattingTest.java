//Adapted from demo from https://guides.gradle.org/building-java-web-applications/#unit_test_the_servlet_using_mockito

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.Keys;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class FlightFormattingTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    @Test
    public void resultsFlightFormatting() throws Exception {      
		//Tests that flights are formatted correctly in results page.
		//Namely, there is an image, The name of departure/destination
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"results?flightType=oneway&departureAirport=ATL&arrivalAirport=GIG&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("f-logo")));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Rio De Janeiro"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"ATL-MIA-GIG"));
    }

    @Test
    public void bookingsFlightFormatting() throws Exception {      
		//Tests that flights are formatted correctly on bookings page.
		//Namely, there is an image, The name of departure/destination
		//Books a flight for test@test.com
		bookFlightBookings();
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"bookings?email=test%40test.com");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("f-logo")));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Rio De Janeiro"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"ATL-MIA"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"MIA-GIG"));
    }
	

    @Test
    public void paymentFlightFormatting() throws Exception {      
		//Tests that flights are formatted correctly on payment page.
		//Namely, there is an image, The name of departure/destination
		//Books a flight for test@test.com
		bookFlightBookings();
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"payment?email=test@test.com");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("f-logo")));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Rio De Janeiro"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"ATL-MIA"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"MIA-GIG"));
    }
	
    @Test
    public void purchaseHistoryFlightFormatting() throws Exception {      
		//Tests that flights are formatted correctly on purchase History page.
		//Namely, there is an image, The name of departure/destination
		//Purchases flight using following function:
		purchaseFlightBookings();
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"purchase-history?email=test%40test.com");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("f-logo")));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"Rio De Janeiro"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"ATL-MIA"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("f-table"),"MIA-GIG"));
    }

	public void bookFlightBookings() throws Exception{
		//Books a flight for Test@test.com
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("ATL");
		driver.findElement(By.id("arrivalAirport")).sendKeys("GIG");
		driver.findElement(By.id("departureDate")).sendKeys("24122017");
		driver.findElement(By.id("searchInfo")).submit();
		//Waits until "Mon, Dec 25" is found somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.linkText("View"),"View"));
		driver.findElement(By.linkText("View")).click();
		//Goes to view flight details page
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Book")));
		driver.findElement(By.linkText("Book")).click();
		//Books the flight ticket for test@test.com
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bookseat")));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("bookseat")).click();
	}
	
	public void purchaseFlightBookings() throws Exception{
		//purchases a flight for Test@test.com
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("ATL");
		driver.findElement(By.id("arrivalAirport")).sendKeys("GIG");
		driver.findElement(By.id("departureDate")).sendKeys("24122017");
		driver.findElement(By.id("searchInfo")).submit();
		//Waits until "Mon, Dec 25" is found somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.linkText("View"),"View"));
		driver.findElement(By.linkText("View")).click();
		//Goes to view flight details page
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Book")));
		driver.findElement(By.linkText("Book")).click();
		//Books the flight ticket for test@test.com
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bookseat")));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("bookseat")).click();
		//Clicking on bookseat sends to bookings page - proceed to payment
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Proceed to Payment")));
		driver.findElement(By.linkText("Proceed to Payment")).click();
		//Enters payment details
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Payment"));
		driver.findElement(By.id("email")).sendKeys("test@test.com");
		driver.findElement(By.id("nameOnCard")).sendKeys("Test McTest");
		driver.findElement(By.id("creditCardNumber")).sendKeys("3234");
		driver.findElement(By.id("confirm")).click();
	}
}