//Adapted from demo from https://guides.gradle.org/building-java-web-applications/#unit_test_the_servlet_using_mockito

package uitesting;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
/*
import org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
*/
import org.openqa.selenium.chrome.ChromeDriver;
import seng3150.group3.Utility;

import static org.junit.Assert.assertEquals;

public class SearchFlightTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup(); 
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();               
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();                         
    }

    @Test
    public void ATLGIGSearch() throws Exception {   
		//Searches directly in URL for a flight from ATL to GIG on 24th/Dec/2017
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"results?flightType=oneway&departureAirport=ATL&arrivalAirport=GIG&departureDate=2017-12-24&returnDate=&passengers=1&flightClass=ECO");
		//Searches for "Atlanta" and "Rio De Janerio" somewhere in the body.
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Atlanta"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Rio De Janeiro"));
    }
	@Test
    public void LGAORDSearch() throws Exception {   
		//Inputs search keys into search form and finds valid search results
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("LGA");
		driver.findElement(By.id("arrivalAirport")).sendKeys("ORD");
		driver.findElement(By.id("departureDate")).sendKeys("12012018");
		driver.findElement(By.id("searchInfo")).submit();
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"New York - Laguardia"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Chicago - OHare Intl."));
    }
	@Test
    public void inputSearch() throws Exception {   
		//Inputs search keys into search form and finds valid search results
		WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"search");
		//Sends keys to elements
		driver.findElement(By.id("departureAirport")).sendKeys("ATL");
		driver.findElement(By.id("arrivalAirport")).sendKeys("GIG");
		driver.findElement(By.id("departureDate")).sendKeys("24122017");
		driver.findElement(By.id("searchInfo")).submit();
		//Waits until "Mon, Dec 25" is found somewhere in the body.
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("body"),"Mon, Dec 25"));
    }
}