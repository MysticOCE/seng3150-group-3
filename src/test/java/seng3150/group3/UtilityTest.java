package seng3150.group3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import seng3150.group3.beans.*;
import seng3150.group3.service.FlightDAOI;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static seng3150.group3.Utility.*;

public class UtilityTest {
    @Mock
    private FlightDAOI flightMock;

    @InjectMocks
    private Utility utility;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    // @After
    // public void tearDown() {}

    @Test
    public void getMultiFlightTest() throws Exception {
        /* CREATE FLIGHTS TO BE RETURNED BY MOCKED FlightDAOI */
        Flights flightA = getDummyFlight(1);  // ATL-MIA 2017-10-23
        Flights flightB = getDummyFlight(2);  // MIA-GIG 2017-10-23

        /* DEFINE MOCKED METHOD(S) */
        when(flightMock.getFlight(1)).thenReturn(flightA);
        when(flightMock.getFlight(2)).thenReturn(flightB);

        /* CALL METHOD TO BE TESTED */
        LinkedList<Flights> multiFlight = utility.getMultiFlight("1,2");

        /* ENSURE ALL VALUES ARE AS EXPECTED */
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");

        assertEquals("AA", multiFlight.get(0).getAirlineCode());
        assertEquals("AA1735", multiFlight.get(0).getFlightNumber());
        assertEquals(df.parse("2017-10-23T09:50Z"), multiFlight.get(0).getDepartureTime());
        assertEquals(10506, multiFlight.get(0).getId());
        assertEquals("ATL", multiFlight.get(0).getDepartureCodeString());
        assertEquals(120, multiFlight.get(0).getDuration());
        assertEquals(df.parse("2017-10-23T11:50Z"), multiFlight.get(0).getArrivalTime());
        assertEquals("MIA", multiFlight.get(0).getDestinationCodeString());
        assertEquals("A380", multiFlight.get(0).getPlaneCode());

        assertEquals("ATL", multiFlight.get(0).getDepartureCode().getDestinationCode());
        assertEquals("Atlanta", multiFlight.get(0).getDepartureCode().getAirport());
        assertEquals("USA", multiFlight.get(0).getDepartureCode().getCountryCode3());

        assertEquals("MIA", multiFlight.get(0).getDestinationCode().getDestinationCode());
        assertEquals("Miami", multiFlight.get(0).getDestinationCode().getAirport());
        assertEquals("USA", multiFlight.get(0).getDestinationCode().getCountryCode3());

        assertEquals("AA", multiFlight.get(0).getAirline().getAirlineCode());
        assertEquals("American Airlines", multiFlight.get(0).getAirline().getAirlineName());

        assertEquals("USA", multiFlight.get(0).getAirline().getCountry().getCountryCode3());
        assertEquals("US", multiFlight.get(0).getAirline().getCountry().getCountryCode2());
        assertEquals("United States of America", multiFlight.get(0).getAirline().getCountry().getAlternateName1());
        assertEquals("U.S.A.", multiFlight.get(0).getAirline().getCountry().getAlternateName2());

        assertEquals("A380", multiFlight.get(0).getPlaneType().getPlaneCode());
        assertEquals("Airbus A380", multiFlight.get(0).getPlaneType().getDetails());
        assertEquals(46, multiFlight.get(0).getPlaneType().getNumFirstClass());
        assertEquals(47, multiFlight.get(0).getPlaneType().getNumBusiness());
        assertEquals(111, multiFlight.get(0).getPlaneType().getNumPremiumEconomy());
        assertEquals(203, multiFlight.get(0).getPlaneType().getNumEconomy());


        assertEquals("AA", multiFlight.get(1).getAirlineCode());
        assertEquals("AA1735", multiFlight.get(1).getFlightNumber());
        assertEquals(df.parse("2017-10-23T23:20Z"), multiFlight.get(1).getDepartureTime());
        assertEquals(30505, multiFlight.get(1).getId());
        assertEquals("MIA", multiFlight.get(1).getDepartureCodeString());
        assertEquals(520, multiFlight.get(1).getDuration());
        assertEquals(df.parse("2017-10-24T09:00Z"), multiFlight.get(1).getArrivalTime());
        assertEquals("GIG", multiFlight.get(1).getDestinationCodeString());
        assertEquals("A380", multiFlight.get(1).getPlaneCode());

        assertEquals("MIA", multiFlight.get(1).getDepartureCode().getDestinationCode());
        assertEquals("Miami", multiFlight.get(1).getDepartureCode().getAirport());
        assertEquals("USA", multiFlight.get(1).getDepartureCode().getCountryCode3());

        assertEquals("GIG", multiFlight.get(1).getDestinationCode().getDestinationCode());
        assertEquals("Rio De Janeiro", multiFlight.get(1).getDestinationCode().getAirport());
        assertEquals("BRA", multiFlight.get(1).getDestinationCode().getCountryCode3());

        assertEquals("AA", multiFlight.get(1).getAirline().getAirlineCode());
        assertEquals("American Airlines", multiFlight.get(1).getAirline().getAirlineName());

        assertEquals("USA", multiFlight.get(1).getAirline().getCountry().getCountryCode3());
        assertEquals("US", multiFlight.get(1).getAirline().getCountry().getCountryCode2());
        assertEquals("United States of America", multiFlight.get(1).getAirline().getCountry().getAlternateName1());
        assertEquals("U.S.A.", multiFlight.get(1).getAirline().getCountry().getAlternateName2());

        assertEquals("A380", multiFlight.get(1).getPlaneType().getPlaneCode());
        assertEquals("Airbus A380", multiFlight.get(1).getPlaneType().getDetails());
        assertEquals(46, multiFlight.get(1).getPlaneType().getNumFirstClass());
        assertEquals(47, multiFlight.get(1).getPlaneType().getNumBusiness());
        assertEquals(111, multiFlight.get(1).getPlaneType().getNumPremiumEconomy());
        assertEquals(203, multiFlight.get(1).getPlaneType().getNumEconomy());
    }

    @Test
    public void getFlightInfoTest() throws Exception {
        Flights flight1A = getDummyFlight(1);
        Flights flight1B = getDummyFlight(2);
        Flights flight2 = getDummyFlight(3);

        List<List<Flights>> flightList = new LinkedList<List<Flights>>();
        List<Flights> multiFlight1 = new LinkedList<Flights>();
        List<Flights> multiFlight2 = new LinkedList<Flights>();

        multiFlight1.add(flight1A);
        multiFlight1.add(flight1B);
        multiFlight2.add(flight2);
        flightList.add(multiFlight1);
        flightList.add(multiFlight2);

        List<FlightInfo> flightInfos = getFlightInfo(flightList);

        assertEquals("American Airlines", flightInfos.get(0).getAirlineName());
        assertEquals("AA1735", flightInfos.get(0).getFlightNumber());
        assertEquals("09:50 AM", flightInfos.get(0).getDepartureTime());  // Alternatively -- "9:50 AM"
        assertEquals("Mon, Oct 23", flightInfos.get(0).getDepartureDate());
        assertEquals("Atlanta", flightInfos.get(0).getDepartureCity());
        assertEquals("22h 10min", flightInfos.get(0).getDuration());
        assertEquals("09:00 AM", flightInfos.get(0).getArrivalTime());
        assertEquals("Tue, Oct 24", flightInfos.get(0).getArrivalDate());
        assertEquals("Rio De Janeiro", flightInfos.get(0).getDestinationCity());
        assertEquals("A380", flightInfos.get(0).getPlaneCode());
        assertEquals("ATL-MIA-GIG", flightInfos.get(0).getAirports());
        assertEquals("img/AA.png", flightInfos.get(0).getImgsrc());
        assertEquals("1 stop", flightInfos.get(0).getStops());
        assertEquals("10506,30505", flightInfos.get(0).getId());
    }

    @Test
    public void getLegsTest() throws Exception {
        Flights flightA = getDummyFlight(1);  // ATL-MIA 2017-10-23
        Flights flightB = getDummyFlight(2);  // MIA-GIG 2017-10-23

        List<Flights> multiFlight = new LinkedList<Flights>();
        multiFlight.add(flightA);
        multiFlight.add(flightB);

        List<FlightLeg> legs = getLegs(multiFlight);
        assertEquals("2h 0min", legs.get(0).getDuration());  // Alternatively -- "2h"?
        assertEquals(false, legs.get(0).isStopover());       // I think "2h 0min" looks better
        assertEquals("Mon 09:50", legs.get(0).getDepartureDayTime());
        assertEquals("Miami", legs.get(0).getDestinationCity());
        assertEquals("MIA", legs.get(0).getDestinationCode());
        assertEquals("Mon 11:50", legs.get(0).getArrivalDayTime());
        assertEquals("Atlanta", legs.get(0).getDepartureCity());
        assertEquals("ATL", legs.get(0).getDepartureCode());
        assertEquals("American Airlines", legs.get(0).getAirlineName());
        assertEquals("AA1735", legs.get(0).getFlightNumber());
        assertEquals("img/AA.png", legs.get(0).getImgsrc());
        assertEquals("A380", legs.get(0).getPlaneCode());

        assertEquals("11h 30min", legs.get(1).getDuration());
        assertEquals(true, legs.get(1).isStopover());
        assertEquals("N/A", legs.get(1).getDepartureDayTime());
        assertEquals("Miami", legs.get(1).getDestinationCity());
        assertEquals("MIA", legs.get(1).getDestinationCode());
        assertEquals("N/A", legs.get(1).getArrivalDayTime());
        assertEquals("Miami", legs.get(1).getDepartureCity());
        assertEquals("MIA", legs.get(1).getDepartureCode());
        assertEquals("N/A", legs.get(1).getAirlineName());
        assertEquals("N/A", legs.get(1).getFlightNumber());
        assertEquals("N/A", legs.get(1).getImgsrc());
        assertEquals("N/A", legs.get(1).getPlaneCode());

        assertEquals("8h 40min", legs.get(2).getDuration());
        assertEquals(false, legs.get(2).isStopover());
        assertEquals("Mon 23:20", legs.get(2).getDepartureDayTime());
        assertEquals("Rio De Janeiro", legs.get(2).getDestinationCity());
        assertEquals("GIG", legs.get(2).getDestinationCode());
        assertEquals("Tue 09:00", legs.get(2).getArrivalDayTime());
        assertEquals("Miami", legs.get(2).getDepartureCity());
        assertEquals("MIA", legs.get(2).getDepartureCode());
        assertEquals("American Airlines", legs.get(2).getAirlineName());
        assertEquals("AA1735", legs.get(2).getFlightNumber());
        assertEquals("img/AA.png", legs.get(2).getImgsrc());
        assertEquals("A380", legs.get(2).getPlaneCode());
    }

    @Test
    public void getLegTest() throws Exception {
        Flights flight = getDummyFlight(1);  // ATL-MIA 2017-10-23

        FlightLeg leg = getLeg(flight);
        assertEquals("2h 0min", leg.getDuration());  // Alternatively -- "2h"?
        assertEquals(false, leg.isStopover());       // I think "2h 0min" looks better
        assertEquals("Mon 09:50", leg.getDepartureDayTime());
        assertEquals("Miami", leg.getDestinationCity());
        assertEquals("MIA", leg.getDestinationCode());
        assertEquals("Mon 11:50", leg.getArrivalDayTime());
        assertEquals("Atlanta", leg.getDepartureCity());
        assertEquals("ATL", leg.getDepartureCode());
        assertEquals("American Airlines", leg.getAirlineName());
        assertEquals("AA1735", leg.getFlightNumber());
        assertEquals("img/AA.png", leg.getImgsrc());
        assertEquals("A380", leg.getPlaneCode());
    }

    @Test
    public void getStopoverLegTest() throws Exception {
        Flights flightA = getDummyFlight(1);  // ATL-MIA 2017-10-23
        Flights flightB = getDummyFlight(2);  // MIA-GIG 2017-10-23

        FlightLeg leg = getStopoverLeg(flightA, flightB);
        assertEquals("11h 30min", leg.getDuration());
        assertEquals(true, leg.isStopover());
        assertEquals("N/A", leg.getDepartureDayTime());
        assertEquals("Miami", leg.getDestinationCity());
        assertEquals("MIA", leg.getDestinationCode());
        assertEquals("N/A", leg.getArrivalDayTime());
        assertEquals("Miami", leg.getDepartureCity());
        assertEquals("MIA", leg.getDepartureCode());
        assertEquals("N/A", leg.getAirlineName());
        assertEquals("N/A", leg.getFlightNumber());
        assertEquals("N/A", leg.getImgsrc());
        assertEquals("N/A", leg.getPlaneCode());
    }

    @Test
    public void durationStringTest() {
        assertEquals("11h 9min", durationString(669));
        assertEquals("0h 46min", durationString(46));  // Alternatively -- "46min"?
    }

    @Test
    public void stopoverMinsTest() {
        assertEquals(75270, stopoverMins(1509886800000L, 1514403000000L));
    }

    @Test
    public void millisecondsToMinsTest() {
        assertEquals(720, millisecondsToMins(720*60*1000));
    }

    @Test
    public void totalDurationTest1() throws Exception {
        Flights flightA = getDummyFlight(1);  // ATL-MIA 2017-10-23
        Flights flightB = getDummyFlight(2);  // MIA-GIG 2017-10-23

        LinkedList<Flights> multiFlight = new LinkedList<Flights>();
        multiFlight.add(flightA);
        multiFlight.add(flightB);
        String duration = totalDuration(multiFlight);
        assertEquals("22h 10min", duration);
    }

    @Test
    public void totalDurationTest2() {
        Date date1 = new Date(1509886800000L);  // Sunday, November 5, 2017 1:00:00 PM GMT
        Date date2 = new Date(1514403000000L);  // Wednesday, December 27, 2017 7:00:00 PM GMT
        String duration = totalDuration(date1, date2);
        assertEquals("1254h 30min", duration);
    }

    @Test
    public void singleFlightInfosTest() throws Exception {
        Flights flight1 = getDummyFlight(2);  // MIA-GIG 2017-10-23
        Flights flight2 = getDummyFlight(3);  // SYD-BNE 2018-12-30

        List<Flights> flights = new LinkedList<Flights>();
        flights.add(flight1);
        flights.add(flight2);

        List<FlightInfo> flightInfos = singleFlightInfos(flights);
        assertEquals("American Airlines", flightInfos.get(0).getAirlineName());
        assertEquals("AA1735", flightInfos.get(0).getFlightNumber());
        assertEquals("11:20 PM", flightInfos.get(0).getDepartureTime());
        assertEquals("Mon, Oct 23", flightInfos.get(0).getDepartureDate());
        assertEquals("Miami", flightInfos.get(0).getDepartureCity());
        assertEquals("8h 40min", flightInfos.get(0).getDuration());
        assertEquals("09:00 AM", flightInfos.get(0).getArrivalTime());
        assertEquals("Tue, Oct 24", flightInfos.get(0).getArrivalDate());
        assertEquals("Rio De Janeiro", flightInfos.get(0).getDestinationCity());
        assertEquals("A380", flightInfos.get(0).getPlaneCode());
        assertEquals("MIA-GIG", flightInfos.get(0).getAirports());
        assertEquals("img/AA.png", flightInfos.get(0).getImgsrc());
        assertEquals("0 stops", flightInfos.get(0).getStops());
        assertEquals("30505", flightInfos.get(0).getId());

        assertEquals("Delta Air Lines", flightInfos.get(1).getAirlineName());
        assertEquals("DL787", flightInfos.get(1).getFlightNumber());
        assertEquals("11:00 AM", flightInfos.get(1).getDepartureTime());
        assertEquals("Sun, Dec 30", flightInfos.get(1).getDepartureDate());
        assertEquals("Sydney", flightInfos.get(1).getDepartureCity());
        assertEquals("1h 30min", flightInfos.get(1).getDuration());
        assertEquals("12:30 PM", flightInfos.get(1).getArrivalTime());
        assertEquals("Sun, Dec 30", flightInfos.get(1).getArrivalDate());
        assertEquals("Brisbane", flightInfos.get(1).getDestinationCity());
        assertEquals("767-400", flightInfos.get(1).getPlaneCode());
        assertEquals("SYD-BNE", flightInfos.get(1).getAirports());
        assertEquals("img/DL.png", flightInfos.get(1).getImgsrc());
        assertEquals("0 stops", flightInfos.get(1).getStops());
        assertEquals("36553", flightInfos.get(1).getId());
    }

    @Test
    public void singleFlightInfoTest() throws Exception {
        Flights flight1 = getDummyFlight(2);  // MIA-GIG 2017-10-23

        FlightInfo flightInfo = singleFlightInfo(flight1);
        assertEquals("American Airlines", flightInfo.getAirlineName());
        assertEquals("AA1735", flightInfo.getFlightNumber());
        assertEquals("11:20 PM", flightInfo.getDepartureTime());
        assertEquals("Mon, Oct 23", flightInfo.getDepartureDate());
        assertEquals("Miami", flightInfo.getDepartureCity());
        assertEquals("8h 40min", flightInfo.getDuration());
        assertEquals("09:00 AM", flightInfo.getArrivalTime());
        assertEquals("Tue, Oct 24", flightInfo.getArrivalDate());
        assertEquals("Rio De Janeiro", flightInfo.getDestinationCity());
        assertEquals("A380", flightInfo.getPlaneCode());
        assertEquals("MIA-GIG", flightInfo.getAirports());
        assertEquals("img/AA.png", flightInfo.getImgsrc());
        assertEquals("0 stops", flightInfo.getStops());
        assertEquals("30505", flightInfo.getId());
        // assertEquals("$2000.00", flightInfo.getPrice());  //TODO Can't test prices yet
    }

    /* For convenience while testing. Reduces code duplication where the same
     * dummy flight can be used in multiple tests */
    private Flights getDummyFlight(int flightID) throws Exception {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");

        Flights flight = null;
        Destinations departure = null;
        Destinations destination = null;
        Airlines airline = null;
        Country country = null;

        switch(flightID) {
            case 1:  // ATL-MIA 2017-10-23
                flight = new Flights(
                    "AA",
                    "AA1735",
                    df.parse("2017-10-23T09:50Z"),
                    10506,
                    "ATL",
                    120,
                    df.parse("2017-10-23T11:50Z"),
                    "MIA",
                    "A380"
                    );
                /* I've left unneeded setters commented out in case they are needed in the future */
                departure = new Destinations();
                departure.setDestinationCode("ATL");
                departure.setAirport("Atlanta");
                departure.setCountryCode3("USA");
                // departure.setDestinationCode1();
                // departure.setDestinationCode2();
                // departure.setDepartureCodeFlight();
                // departure.setDestinationCodeFlight();
                destination = new Destinations();
                destination.setDestinationCode("MIA");
                destination.setAirport("Miami");
                destination.setCountryCode3("USA");
                // destination.setDestinationCode1();
                // destination.setDestinationCode2();
                // destination.setDepartureCodeFlight();
                // destination.setDestinationCodeFlight();
                airline = new Airlines();
                airline.setAirlineCode("AA");
                airline.setAirlineName("American Airlines");
                // airline.setCountry();
                // airline.setFlights();
                country = new Country();
                country.setCountryCode3("USA");
                country.setCountryCode2("US");
                country.setAlternateName1("United States of America");
                country.setAlternateName2("U.S.A.");
                // country.setMotherCountryCode3();
                // country.setMotherCountryComment();
                // country.setAirlines();
                // country.setDestinations();
                airline.setCountry(country);
                // airline.setFlights();

                flight.setDepartureCode(departure);
                flight.setDestinationCode(destination);
                flight.setAirline(airline);
                flight.setPlaneType(new PlaneType("A380", "Airbus A380", 46, 47, 111, 203));

                return flight;
            case 2:  // MIA-GIG 2017-10-23
                flight = new Flights(
                    "AA",
                    "AA1735",
                    df.parse("2017-10-23T23:20Z"),
                    30505,
                    "MIA",
                    520,
                    df.parse("2017-10-24T09:00Z"),
                    "GIG",
                    "A380"
                    );
                /* I've left unneeded setters commented out in case they are needed in the future */
                departure = new Destinations();
                departure.setDestinationCode("MIA");
                departure.setAirport("Miami");
                departure.setCountryCode3("USA");
                // departure.setDestinationCode1();
                // departure.setDestinationCode2();
                // departure.setDepartureCodeFlight();
                // departure.setDestinationCodeFlight();
                destination = new Destinations();
                destination.setDestinationCode("GIG");
                destination.setAirport("Rio De Janeiro");
                destination.setCountryCode3("BRA");
                // destination.setDestinationCode1();
                // destination.setDestinationCode2();
                // destination.setDepartureCodeFlight();
                // destination.setDestinationCodeFlight();
                airline = new Airlines();
                airline.setAirlineCode("AA");
                airline.setAirlineName("American Airlines");
                // airline.setCountry();
                // airline.setFlights();
                country = new Country();
                country.setCountryCode3("USA");
                country.setCountryCode2("US");
                country.setAlternateName1("United States of America");
                country.setAlternateName2("U.S.A.");
                // country.setMotherCountryCode3();
                // country.setMotherCountryComment();
                // country.setAirlines();
                // country.setDestinations();
                airline.setCountry(country);
                // airline.setFlights();

                flight.setDepartureCode(departure);
                flight.setDestinationCode(destination);
                flight.setAirline(airline);
                flight.setPlaneType(new PlaneType("A380", "Airbus A380", 46, 47, 111, 203));

                return flight;
            case 3:  // SYD-BNE 2018-12-30
                flight = new Flights(
                    "DL",
                    "DL787",
                    df.parse("2018-12-30T11:00Z"),
                    36553,
                    "SYD",
                    90,
                    df.parse("2018-12-30T12:30Z"),
                    "BNE",
                    "767-400"
                    );
                departure = new Destinations();
                departure.setDestinationCode("SYD");
                departure.setAirport("Sydney");
                departure.setCountryCode3("AUS");
                // departure.setDestinationCode1();
                // departure.setDestinationCode2();
                // departure.setDepartureCodeFlight();
                // departure.setDestinationCodeFlight();
                destination = new Destinations();
                destination.setDestinationCode("BNE");
                destination.setAirport("Brisbane");
                destination.setCountryCode3("AUS");
                // destination.setDestinationCode1();
                // destination.setDestinationCode2();
                // destination.setDepartureCodeFlight();
                // destination.setDestinationCodeFlight();
                airline = new Airlines();
                airline.setAirlineCode("DL");
                airline.setAirlineName("Delta Air Lines");
                // airline.setCountry();
                // airline.setFlights();
                country = new Country();
                country.setCountryCode3("AUS");
                country.setCountryCode2("AU");
                country.setAlternateName1("Commonwealth of Australia");
                // country.setAlternateName2("");
                // country.setMotherCountryCode3();
                // country.setMotherCountryComment();
                // country.setAirlines();
                // country.setDestinations();
                airline.setCountry(country);
                // airline.setFlights();

                flight.setDepartureCode(departure);
                flight.setDestinationCode(destination);
                flight.setAirline(airline);
                flight.setPlaneType(new PlaneType("767-400", "Boeing 767-400", 42, 50, 121, 220));

                return flight;
            default:
                throw new Exception("Unsupported integer given to getDummyFlight.");
        }
    }
}
