package controllertest;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import seng3150.group3.Utility;

public class FlightDetailsTest {

    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }
    @Test
    public void checkFlightDetails(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root + "flight-details?id=10478");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h2"),"Flight Details"));
        driver.quit();
    }
    @Test
    public void indexTest(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Search"));
        Assert.assertEquals("Search | FlightPub", driver.getTitle());
        driver.quit();
    }
    @Test
    public void purchaseSummaryTest(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root + "purchase-summary?email=r0ck@hotmail.com");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"), "Purchase Summary"));
        driver.quit();


    }
}

