package controllertest;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import seng3150.group3.Utility;

//Class coers AddTagController, TagController, ViewReviews, ViewReviewsAdmin, CreateReview
public class ReviewTagTest {
    private WebDriver driver;
    private String root = Utility.getTestRoot();

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }

    @Before
    public void setUp() {
        driver = new ChromeDriver();
    }

    @After
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }

    @Test
    public void createReviewWithTags(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+ "create-review?flight=30565");
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.id("comment")));
        //we are now at the correct page
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Create Review for AA1735"));
        driver.findElement(By.id("comment")).sendKeys("Test review for tags");
        driver.findElement(By.id("email")).sendKeys("r0ck@hotmail.com");
        driver.findElement(By.id("password")).sendKeys("kangaroo");
        driver.findElement(By.id(("submitButton"))).click();
        //navigated to add tags page
        Select tagName = new Select(driver.findElement(By.name("tagName")));
        tagName.selectByVisibleText("Food");
        //Select tagScore = new Select(driver.findElement(By.name("tagScore")));
        //tagScore.selectByVisibleText("4");
        driver.findElement(By.id(("submitButton"))).click();
    }
    @Test
    public void invalidReviewAttempt(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+ "create-review?flight=30565");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Create Review for AA1735"));
        driver.findElement(By.id("comment")).sendKeys("Bad review for validation");
        driver.findElement(By.id("email")).sendKeys("notatrueemail");
        driver.findElement(By.id("password")).sendKeys("anythingreally");
        driver.findElement(By.id("submitButton")).click();
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"), "Search"));

    }


    @Test
    public void viewReviewsForSingleFlight(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+"view-reviews?flight=10478");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"),"Reviews for AA1735 | FlightPub"));
        driver.quit();

    }

    @Test
    public void addAndDeleteTagNames(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.get(root+ "view-reviews-admin?flight=10478");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"), "Reviews for Flight AA1735 | FlightPub"));
        driver.findElement(By.id("inputTagName")).sendKeys("testingtag");
        driver.findElement(By.id("submitTag")).click();
        driver.get(root+"view-reviews-admin?flight=10478");
        driver.findElement(By.id("testingtagbutton")).click();

    }


}
