<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Create Review | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Create Review for ${flight.flightNumber}</h1>
			<!-- TODO Should probably redirect to a page where you can view the
			review you just submitted or all of the reviews for that airline -->
			<!-- Testing again, do form tags compile to HTML? -->
			<form:form action="create-review" method="POST" modelAttribute="review">
				<form:label path="comment">Review</form:label><br>
				<form:textarea path="comment"/><br>
				<form:label path="email">Email</form:label><br>
				<form:input path="email"></form:input><br>
				<form:label path="password">Password</form:label><br>
				<form:input path="password"></form:input>
				&nbsp;&nbsp;
				<br><br>
				<form:hidden path="flightID" value="${flight.id}"/>
				<button class="button" type="submit" id="submitButton">Submit</button>
			</form:form>
		</div>
	</body>
</html>
