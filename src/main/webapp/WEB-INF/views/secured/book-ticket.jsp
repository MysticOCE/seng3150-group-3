<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Book Ticket | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<div class="detail-view-header">
				<h2>Book Ticket</h2>
			</div>
			<div class="f-table">
				<c:forEach var="leg" items="${legs}">
					<div class="f-row f-row-detail-view">
						<div class="f-cell">${leg.duration}</div>
						<div class="f-cell">
							<c:choose>
								<c:when test="${leg.stopover}">
									Stopover in ${leg.departureCity} (${leg.departureCode})
								</c:when>
								<c:otherwise>
									<div class="f-info">
										${leg.departureCity} (${leg.departureCode})
										<span class="f-time"><br>${leg.departureDayTime}</span>
									</div>
									<div class="f-separator">&rarr;</div>
									<div class="f-info">
										${leg.destinationCity} (${leg.destinationCode})
										<span class="f-time"><br>${leg.arrivalDayTime}</span>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:forEach>
			</div>
			<p>
				Total Duration: ${totalDuration}
			</p>
			<form action="bookings">
				<label for="email">Email</label>
					<input type="text" name="email" id="email">
				<br>
				<br>
				<input type="hidden" name="add" id="add" value="${id}"><%--value="17" should have flightID in 17--%>
				<label for="class">Ticket Class</label>
				<select name="class" id="class">
					<option value="ECO">Economy</option>
					<option value="PME">Premium Economy</option>
					<option value="BUS">Business</option>
					<option value="FIR">First Class</option>
				</select><br><br>
				<label for="passengers">Passengers</label>
				<select name="passengers" id="passengers">
					<c:forEach var = "i" begin = "1" end = "5">
						<option value="${i}"><c:out value = "${i}"/></option>
					</c:forEach>
				</select>
				<br>
				<br>
				<button id="bookseat" class="button" type="submit">Book seat</button>

				<a id="returnToBookings" class="button" href="bookings">Return to bookings</a>
			</form>
		</div>
	</body>
</html>
