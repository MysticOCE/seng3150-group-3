<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList, seng3150.group3.beans.Tag"%>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
<<<<<<< HEAD
		<title>Reviews for Flight ${flight.flightNumber} | FlightPub</title>
=======
		<title>Reviews | FlightPub</title>
>>>>>>> remotes/origin/user-testing-fixes
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<% int i = 0;%>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Reviews for ${flight.flightNumber} | FlightPub</h1>
			<p> ${select} </p>
			<div class="f-row f-row-header">
				<div class="f-cell">User</div>
				<div class="f-cell">Flight Date and Time</div>
				<div class="f-cell">Comment</div>
				<div class="f-cell">Tags</div>
			</div>
			<c:forEach var="review" items="${reviews}" varStatus="status">
			<div class="f-row">
				<div class="f-cell">${users[status.index].firstName} ${users[status.index].lastName}</div>
				<div class="f-cell">${flight.departureTime}</div>
				<div class="f-cell">${review.comment}</div>
				<div class="f-cell">
				<c:forEach var="tag" items="${taglists[status.index]}">
				${tag.tagName} ${tag.tagScore}
				</c:forEach>
				</div>
			</div>
			</c:forEach>
		<a class="button" href="flight-details?id=<%= request.getParameter("flight") %>">Back</a>
		</div>
		<script>
			function printFunction() {
				window.print();
			}
		</script>
	</body>
</html>
