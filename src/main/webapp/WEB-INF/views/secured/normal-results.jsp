<!-- AJAX page - Not standalone -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>Flight List</h2>
<label for="sortMethod">Sort by</label>
<select name="sortMethod" id="sortMethod">
	<option value="price">Price</option>
	<option value="duration">Duration</option>
	<option value="departure">Departure Time</option>
	<option value="arrival">Arrival Time</option>
	<option value="reviews">User Reviews</option>
</select>
<div class="f-table">
	<c:forEach var="flight" items="${flights}">
		<div class="f-row">
			<div class="f-cell">
				<img alt="<c:out value='${flight.airlineName}' /> Logo" class="f-logo" src="<c:out value='${flight.imgsrc}' />">
			</div>
			<div class="f-cell">
				<div class="f-info">
					<span class="f-city"><c:out value='${flight.departureCity}' /><br></span>
					<c:out value='${flight.departureTime}' />
					<span class="f-time"><br><c:out value='${flight.departureDate}' /></span>
				</div>
				<div class="f-separator">&rarr;</div>
				<div class="f-info">
					<span class="f-city"><c:out value='${flight.destinationCity}' /><br></span>
					<c:out value='${flight.arrivalTime}' />
					<span class="f-time"><br><c:out value='${flight.arrivalDate}' /></span>
				</div>
			</div>
			<div class="f-cell">
				<c:out value='${flight.duration}' /> (<c:out value='${flight.stops}' />)<br>
				<c:out value='${flight.airports}' />
			</div>
			<div class="f-cell"><c:out value='${flight.price}' /></div>
			<div class="f-cell">
				<a class="button" href="flight-details?id=${flight.id}">View</a>
				<a class="button" href="book-ticket?id=${flight.id}">Book</a>
			</div>
		</div>
	</c:forEach>
</div>
