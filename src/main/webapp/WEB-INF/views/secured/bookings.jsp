<!-- AJAX page - Not standalone -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bookings | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Bookings</h1>
			<form action="bookings">
				<label for="email">Email</label>
					<input type="text" name="email" id="email">
				<button class="button" type="submit">Submit</button>
			</form>
			<br>
			<br>
			<div class="f-table">
				<c:forEach var="bookingOrder" items="${bookingsOrderList}">
					<div class="f-row">
						<c:set var = "bookingOrderBID" value = "${bookingOrder.getBookingID()}"/>
						<c:forEach var="booking" items="${bookingsList}">
							<c:set var = "bookingID" value = "${booking.getBookingID()}"/>
							<c:set var = "flightID" value = "${booking.getFlightID()}"/>
							<c:set var = "classcode" value = "${booking.getClassCode()}"/>
							<c:if test = "${bookingOrderBID == bookingID}">
								<c:forEach var="flight" items="${flights}">
									<c:set var = "flight_flightID" value = "${flight.getId()}"/>
									<c:if test = "${flight_flightID == flightID}">
										<div class="f-cell">
											<img alt="<c:out value='${flight.airlineName}' /> Logo" class="f-logo" src="<c:out value='${flight.imgsrc}' />">
										</div>
										<div class="f-cell">
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.departureCity}' /><br></span>
												<c:out value='${flight.departureTime}' />
												<span class="f-time"><br><c:out value='${flight.departureDate}' /></span>
											</div>
											<div class="f-separator">&rarr;</div>
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.destinationCity}' /><br></span>
												<c:out value='${flight.arrivalTime}' />
												<span class="f-time"><br><c:out value='${flight.arrivalDate}' /></span>
											</div>
										</div>
										<div class="f-cell">
											<c:out value='${flight.duration}' /> (<c:out value='${flight.stops}' />)<br>
											<c:out value='${flight.airports}' />
										</div>
										<div class="f-cell"><c:out value='${flight.price}' /></div>
										<c:set var = "view_flightID" value = "${flight.getId()}"/>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
						
						<div class="f-cell">
							<a class="button" href="flight-details?id=${view_flightID}">View</a>
							<a class="button" href="bookings?email=<%= request.getParameter("email") %>&delete=${bookingOrder.getBookingOrderID()}">Delete</a>
							<%-- <button type="button" class="button" href="bookings">Delete</button> --%>
							<%-- <button type="button" class="button">Swap</button> --%>
						</div>
					</div>
				</c:forEach>
			</div>

				
			<br>
			<a class="button" href="search">Add Flight</a>
			<br><br>
			<a class="button" href="payment?email=<%= request.getParameter("email") %>">Proceed to Payment</a>
		</div>
	</body>
</html>
