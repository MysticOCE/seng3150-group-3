<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!-- In final implementation all dynamic values will use c:out tags for security -->
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Flight Details | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<div class="detail-view-header">
				<h2>Flight Details</h2>
			</div>
			<div class="f-table">
				<c:forEach var="leg" items="${legs}">
					<div class="f-row f-row-detail-view">
						<div class="f-cell">${leg.duration}</div>
						<div class="f-cell">
							<c:choose>
								<c:when test="${leg.stopover}">
									Stopover in ${leg.departureCity} (${leg.departureCode})
								</c:when>
								<c:otherwise>
									<div class="f-info">
										${leg.departureCity} (${leg.departureCode})
										<span class="f-time"><br>${leg.departureDayTime}</span>
									</div>
									<div class="f-separator">&rarr;</div>
									<div class="f-info">
										${leg.destinationCity} (${leg.destinationCode})
										<span class="f-time"><br>${leg.arrivalDayTime}</span>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:forEach>
			</div>
			<p>
				Total Duration: ${totalDuration}
			</p>
			<a class="button" href="bookings">Back</a><!--TODO map to the last page visited-->
			<a class="button" href="book-ticket?id=${id}">Book</a>
            <c:forEach var="flight" items="${flights}" varStatus="status">
                <a class="button" href="view-reviews?flight=${flight.id}">Reviews for flight ${status.count}</a>
            </c:forEach>
			<br>
		</div>
	</body>
</html>
