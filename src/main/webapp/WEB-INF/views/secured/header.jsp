<!-- jsp:include page - Not standalone -->
<header>
	<a href="search"><img class="header-logo" src="img/flightpub-logo.png" alt="FlightPub"></a>
	<div class="header-buttons">
		<a class="button" href="search">Search</a>
		<a class="button" href="purchase-history">Past&nbsp;Flights</a>
		<a class="button" href="bookings">Bookings</a>
		<a class="button" href="logout">Logout</a>
	</div>
</header>
