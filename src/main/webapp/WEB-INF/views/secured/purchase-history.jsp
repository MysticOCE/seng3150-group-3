<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Purchase History | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Purchase History</h1>
			<form action="purchase-history">
				<label for="email">Email</label>
					<input type="text" name="email" id="email">
				<button class="button" type="submit">Submit</button>
			</form>
			<div class="f-table">
				<c:forEach var="bookingHistory" items="${purchaseHistory}">
					<div class="f-row">
						<c:set var = "bookingOrderBID" value = "${bookingHistory.getBookingID()}"/>
						<c:forEach var="booking" items="${bookingsList}">
							<c:set var = "bookingID" value = "${booking.getBookingID()}"/>
							<c:set var = "flightID" value = "${booking.getFlightID()}"/>
							<c:set var = "classcode" value = "${booking.getClassCode()}"/>
							<c:if test = "${bookingOrderBID == bookingID}">
								<c:forEach var="flight" items="${flights}">
									<c:set var = "flight_flightID" value = "${flight.getId()}"/>
									<c:if test = "${flight_flightID == flightID}">
										<div class="f-cell">
											<img alt="<c:out value='${flight.airlineName}' /> Logo" class="f-logo" src="<c:out value='${flight.imgsrc}' />">
										</div>
										<div class="f-cell">
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.departureCity}' /><br></span>
												<c:out value='${flight.departureTime}' />
												<span class="f-time"><br><c:out value='${flight.departureDate}' /></span>
											</div>
											<div class="f-separator">&rarr;</div>
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.destinationCity}' /><br></span>
												<c:out value='${flight.arrivalTime}' />
												<span class="f-time"><br><c:out value='${flight.arrivalDate}' /></span>
											</div>
										</div>
										<div class="f-cell">
											<c:out value='${flight.duration}' /> (<c:out value='${flight.stops}' />)<br>
											<c:out value='${flight.airports}' />
										</div>
										<div class="f-cell"><c:out value='${flight.price}' /></div>
										<c:set var = "view_flightID" value = "${flight.getId()}"/>
										
										<div class="f-cell">
											<a class="button" href="flight-details?id=${flight_flightID}">View</a>
											<a class="button" href="create-review?flight=${flight_flightID}">Create Review</a>
										</div>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
		</div>
	</body>
</html>
