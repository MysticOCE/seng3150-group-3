<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Payment | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Payment</h1>
			<h2>Booking Summary</h2>
			<div class="f-table">
				<c:forEach var="bookingOrder" items="${bookingsOrderList}">
					<div class="f-row">
						<c:set var = "bookingOrderBID" value = "${bookingOrder.getBookingID()}"/>
						<c:forEach var="booking" items="${bookingsList}">
							<c:set var = "bookingID" value = "${booking.getBookingID()}"/>
							<c:set var = "flightID" value = "${booking.getFlightID()}"/>
							<c:set var = "classcode" value = "${booking.getClassCode()}"/>
							<c:if test = "${bookingOrderBID == bookingID}">
								<c:forEach var="flight" items="${flights}">
									<c:set var = "flight_flightID" value = "${flight.getId()}"/>
									<c:if test = "${flight_flightID == flightID}">
										<div class="f-cell">
											<img alt="<c:out value='${flight.airlineName}' /> Logo" class="f-logo" src="<c:out value='${flight.imgsrc}' />">
										</div>
										<div class="f-cell">
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.departureCity}' /><br></span>
												<c:out value='${flight.departureTime}' />
												<span class="f-time"><br><c:out value='${flight.departureDate}' /></span>
											</div>
											<div class="f-separator">&rarr;</div>
											<div class="f-info">
												<span class="f-city"><c:out value='${flight.destinationCity}' /><br></span>
												<c:out value='${flight.arrivalTime}' />
												<span class="f-time"><br><c:out value='${flight.arrivalDate}' /></span>
											</div>
										</div>
										<div class="f-cell">
											<c:out value='${flight.duration}' /> (<c:out value='${flight.stops}' />)<br>
											<c:out value='${flight.airports}' />
										</div>
										<div class="f-cell"><c:out value='${flight.price}' /></div>
										<c:set var = "view_flightID" value = "${flight.getId()}"/>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
			<form action="purchase-summary">
				<h2>Payment Details</h2>
				<input type="hidden" name="flights" id="flights" value="${sendflights}">
				<label for="email">Confirm Email</label>
					<input type="text" name="email" id="email">
				<br>
				<br>
				<label for="paymentType">Payment type</label>
				<select name="paymentType" id="paymentType" onchange="hideShow()">
					<option value="creditCard">Credit Card</option>
					<option value="paypal">PayPal</option>
				</select>
				<br>
				<br>
				<div id="paypal">
					<a class="button" href="https://www.paypal.com/au/home">Pay with Paypal</a>
				</div>
				<div id="creditCardInfo">
					<label for="nameOnCard">Name on card</label>
					<input type="text" name="nameOnCard" id="nameOnCard">
					<label for="creditCardNumber">Credit Card Number</label>
					<input type="number" name="creditCardNumber" id="creditCardNumber">
					<p>Credit Expiry Date</p>
					<label for="creditCardExpiryMonth">Month</label>
					<select name="creditCardExpiryMonth" id="creditCardExpiryMonth">
						<option value="jan">01</option>
						<option value="feb">02</option>
						<option value="mar">03</option>
						<option value="apr">04</option>
						<option value="may">05</option>
						<option value="jun">06</option>
						<option value="jul">07</option>
						<option value="aug">08</option>
						<option value="sep">09</option>
						<option value="oct">10</option>
						<option value="nov">11</option>
						<option value="dec">12</option>
					</select>
					<label for="creditCardExpiryYear">Year</label>
					<select name="creditCardExpiryYear" id="creditCardExpiryYear">
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
						<option value="2024">2024</option>
						<option value="2025">2025</option>
						<option value="2026">2026</option>
						<option value="2027">2027</option>
						<option value="2028">2028</option>
					</select>
				</div>
				<script>
					function hideShow() {
						var y = document.getElementById("paypal");
						y.style.display = "none";
						var x = document.getElementById("creditCardInfo");
						x.style.display = "none";
						var getBox = document.getElementById("paymentType");
						var getBoxSelected = getBox.options[getBox.selectedIndex].value;

						if(getBoxSelected ==="paypal") {
							y.style.display = "block";
						}
						else {
							x.style.display = "block";

						}
					}
					window.onload = hideShow;
					/*
					var y = document.getElementById("paypal");
					y.style.display = "none";
					function hideShow() {
						var x = document.getElementById("creditCardInfo");
						//var y = document.getElementById("paypal");
						if (x.style.display === "none") {
							x.style.display = "block";
							y.style.display = "none";
						} else {
							x.style.display = "none";
							y.style.display = "block";
						}
					}
					*/
				</script>
				<br><br>
				<button id="confirm" class="button"type="submit">Confirm</button>
				<a id="change" class="button" href="bookings">Change</a>
			</form>
		</div>
	</body>
</html>
