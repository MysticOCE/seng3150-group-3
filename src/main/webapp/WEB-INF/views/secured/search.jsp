<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Search | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body onload="updateFlightType()">
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Search</h1>
			<form:form action="results" method="GET" modelAttribute="searchInfo">
				<label for="flightType">Flight Type</label><br>
				<select name="flightType" id="flightType" onchange="updateFlightType()">
					<option value="oneway" selected="selected">One-Way</option>
					<option value="return">Return</option>
					<%--<option value="multi">Multi-City</option>--%> <%-- Yeah nah --%>
				</select><br>
				<div>
					<!-- <label for="departureAirport">Departure Airport</label> -->
					<form:input path="departureAirport" placeholder="Flying from" type="text" required="required" />
					<!-- <label for="arrivalAirport">Arrival Airport</label> -->
					<form:input path="arrivalAirport" placeholder="To" type="text" required="required" />

					<form:label path="departureDate">Depart</form:label>
					<form:input path="departureDate" placeholder="dd/mm/yyyy" type="date" required="required" /> <%-- Placeholder is for IE support --%>

					<form:label path="returnDate" id="labelReturnDate">Return</form:label>
					<form:input path="returnDate" placeholder="dd/mm/yyyy" type="date" /> <%-- Placeholder is for IE support --%>
					<br>
				</div>
				<!-- <button class="button" type="button">Add flight</button> -->
				<br><br>
				<form:label path="passengers">Passengers</form:label>
				<form:input path="passengers" type="number" value="1" required="required" min="1" /><br/>

				<form:label path="flightClass">Flight Class</form:label>
				<form:select path="flightClass" name="flightClass" id="flightClass">
					<form:option value="ECO">Economy</form:option>
					<form:option value="PME">Premium Economy</form:option>
					<form:option value="BUS">Business</form:option>
					<form:option value="FIR">First Class</form:option>
				</form:select>
				<br><br>
				<input type="submit" value="Search" />
			</form:form>
		</div>
	</body>
</html>
