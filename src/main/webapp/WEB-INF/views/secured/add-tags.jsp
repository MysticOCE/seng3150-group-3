<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Add Tags | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Review Tags</h1>
			<div>
				<form:form action="add-tags" method="POST" modelAttribute="tag">
					<form:hidden path="reviewID" value="${review.reviewID}"/>
					<form:label path="tagName">Tag: </form:label>
					<form:select path="tagName" items="${names}"/>
					<form:label path="tagScore">Score: </form:label>
					<form:select path="tagScore" items="${scores}"/>
					<input id="submitButton" type="submit" value="Submit"/>
				</form:form>
				<a href="search"><button type="button">Done</button></a>
			</div>
			<div class="f-table">
				<div class="f-row f-row-header">
					<div class="f-cell">Tag</div>
					<div class="f-cell">Rating</div>
				</div>
				<c:forEach items="${tags}" var="indivTag">
					<div class="f-row">
						<div class="f-cell">${indivTag.tagName}</div>
						<div class="f-cell">${indivTag.tagScore}</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</body>
</html>
