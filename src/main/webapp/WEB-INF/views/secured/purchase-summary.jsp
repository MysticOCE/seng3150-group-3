<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Purchase Summary | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Purchase Summary</h1>
			<div class="f-table">
				<c:set var = "totalPrice" value = "0"/>
				<c:forEach var="flight" items="${flights}">
					<div class="f-row">
						<div class="f-cell">
							<img alt="<c:out value='${flight.airlineName}' /> Logo" class="f-logo" src="<c:out value='${flight.imgsrc}' />">
						</div>
						<div class="f-cell">
							<div class="f-info">
								<span class="f-city"><c:out value='${flight.departureCity}' /><br></span>
								<c:out value='${flight.departureTime}' />
								<span class="f-time"><br><c:out value='${flight.departureDate}' /></span>
							</div>
							<div class="f-separator">&rarr;</div>
							<div class="f-info">
								<span class="f-city"><c:out value='${flight.destinationCity}' /><br></span>
								<c:out value='${flight.arrivalTime}' />
								<span class="f-time"><br><c:out value='${flight.arrivalDate}' /></span>
							</div>
						</div>
						<div class="f-cell">
							<c:out value='${flight.duration}' /> (<c:out value='${flight.stops}' />)<br>
							<c:out value='${flight.airports}' />
						</div>
						<div class="f-cell"><c:out value='${flight.price}' /></div>
					</div>
				</c:forEach>
			</div>
			<a class="button" href="search">Finish</a>
			<a class="button" href="purchase-summary?flights=<%= request.getParameter("flights") %>&email=<%= request.getParameter("email") %>&sendemail=1">Email</a>
			<a class="button" onclick="printFunction()" href="">Print</a>
			<script>
				function printFunction() {
					window.print();
				}
			</script>
		</div>
	</body>
</html>
