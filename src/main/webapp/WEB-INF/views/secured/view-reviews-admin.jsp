<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reviews for ${flight.flightNumber} | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Reviews for Flight ${flight.flightNumber} | FlightPub</h1>
			<div class="f-table">
				<div class="f-row f-row-header">
					<div class="f-cell">User</div>
					<div class="f-cell">Flight Date and Time</div>
					<div class="f-cell">Comment</div>
					<div class="f-cell">Tags</div>
					<div class="f-cell">Delete</div>
				</div>
				<c:forEach items="${reviews}" var="review" varStatus="status">
				<div class="f-row">
					<div class="f-cell">${users[status.index].firstName} ${users[status.index].lastName}</div>
					<div class="f-cell">${flight.departureTime}</div>
					<div class="f-cell">${review.comment}</div>
					<div class="f-cell">
					<c:forEach var="tag" items="${tags[status.index]}">
					${tag.tagName} ${tag.tagScore}
					</c:forEach>
					</div>
					<div class="f-cell">
						<spring:url value="/view-reviews-admin/deleteReview${targetReview}" var="deleteReview">
							<spring:param name="targetReview" value="${review.reviewID}"/>
						</spring:url>
						<a href="${deleteReview}" id="${deleteReview}delete"><button type="button">Delete</button></a>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}"/>
					</div>
				</c:forEach>
			</div>
			<!-- Adding new tag types to table -->
			<form:form action="view-reviews-admin" method="POST" modelAttribute="newTag">
				<h2>Add Tags</h2>
				<form:label path="tagName">New Tag:</form:label>
				<form:input path="tagName" id="inputTagName"/> <input type="submit" name="addTag" value="Add Tag" id="submitTag"/>
			</form:form>
			<!-- EOF of add tag -->
			<!--Deleting tag names from table -->
			<div class="f-table">
				<div class="f-row f-row-header">
					<div class="f-cell">Current Tags</div>
					<div class="f-cell">Delete</div>
				</div>
				<c:forEach items="${tagNames}" var="tagName">
				<div class="f-row">
					<div class="f-cell">${tagName.tagName}</div>
					<div class="f-cell">
						<!-- using spring param to avoid injection attacks -->
						<spring:url value="/deleteTag${tag}" var="deleteURL">
							<spring:param name="tag" value="${tagName.tagName}"/>
						</spring:url>
						<a href="${deleteURL}" id="{deleteURL}button"><button type="button">Delete</button></a>
					</div>
				</div>
				</c:forEach>
			</div>
			<br>
			<a class="button" href="flight-details">Back</a>
			<script>
				function printFunction() {
					window.print();
				}
			</script>
		</div>
	</body>
</html>
