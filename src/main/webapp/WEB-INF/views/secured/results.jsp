<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Results | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body onload="showResults()">
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Results</h1>
			<br>
			<hr class="main-divider">
			<div id="recommendations"></div>
			<div id="normal-results"></div>
		</div>
	</body>
</html>
