<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Login</h1>
			<form name ='f' action="login" method="POST">
				<label for="username">Email</label><br>
				<input type="text" name="username" id="username">
				<br>
				<label for="password">Password</label><br>
				<input type="password" name="password" id="password">
				<br><br>
				<input class="button" type="submit" name="submit" value="Submit"/>
			</form>
			<p><c:out value='${invalid}' /></p>
		</div>
	</body>
</html>
