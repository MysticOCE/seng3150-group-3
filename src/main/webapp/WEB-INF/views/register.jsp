<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Register | FlightPub</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<jsp:include page="/WEB-INF/views/secured/header.jsp" flush="true" />
		<div class="main">
			<h1>Register</h1>
			<form action="register" method="POST">
				<label for="password">Password</label><br>
				<input type="text" name="password" id="password">
				<br>
				<label for="password2">Re-enter Password</label><br>
				<input type="text" name="password2" id="password2">
				<br>
				<label for="name">Full Name</label><br>
				<input type="text" name="name" id="name">
				<br>
				<label for="email">Email</label><br>
				<input type="text" name="email" id="email">
				<br><br>
				<input type="submit" name="submit" value="submit"/>
			</form>
		</div>
	</body>
</html>
