// Shows the recommendations and normal search results on the results page
function showResults() {
    $.ajax({
        type:"GET",
        url: "normal-results" + window.location.search,
        success: function(data) {
            $('#normal-results').html(data);
        }
    });
    $.ajax({
        type:"GET",
        url: "recommendations" + window.location.search,
        success: function(data) {
            $('#recommendations').html(data);
        }
    });
}

// Shows/Hides the return date input on the search page
function updateFlightType() {
    if (document.getElementById("flightType").value == "oneway") {
        document.getElementById("returnDate").style.display = "none";
        document.getElementById("labelReturnDate").style.display = "none";
    }
    else {
        document.getElementById("returnDate").style.display = "inline-block";
        document.getElementById("labelReturnDate").style.display = "inline-block";
    }
    // console.log(window.location.search);
}
