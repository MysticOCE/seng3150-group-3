package seng3150.group3.service;

import org.springframework.stereotype.Repository;
import seng3150.group3.beans.Flights;

import java.util.Date;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

// BROKEN. DO NOT USE. TODO

// @Repository
public class FlightSearchStub implements FlightSearchI {
    public void buildFlightPathGraph() {}

    public List<List<Flights>> search(String departureCode, String arrivalCode, Date departureTime, int passengers, String flightClass) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        List<List<Flights>> flights = new LinkedList<List<Flights>>();

        /*
        List<Flights> flight = new LinkedList<Flights>();
        LocalDateTime departTime1 = LocalDateTime.parse("2017-09-23 09:50:00",format);
        LocalDateTime arriveTime1 = LocalDateTime.parse("2017-09-23 11:50:00",format);
        LocalDateTime departTime2 = LocalDateTime.parse("2017-09-23 23:20:00",format);
        LocalDateTime arriveTime2 = LocalDateTime.parse("2017-09-24 09:00:00",format);
        flight.add(new Flights("AA","AA1735",departTime1,1,"ATL",120,arriveTime1,"MIA","A380"));
        flight.add(new Flights("AA","AA1735",departTime2,2,"MIA",520,arriveTime2,"GIG","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 09:50:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 11:50:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 23:20:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 09:00:00",format);
        flight.add(new Flights("AA","AA1735",departTime1,3,"ATL",120,arriveTime1,"MIA","A380"));
        flight.add(new Flights("AA","AA1735",departTime2,4,"MIA",520,arriveTime2,"GIG","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 09:50:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 11:50:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 23:20:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 09:00:00",format);
        flight.add(new Flights("AA","AA1735",departTime1,5,"ATL",120,arriveTime1,"MIA","A380"));
        flight.add(new Flights("AA","AA1735",departTime2,6,"MIA",520,arriveTime2,"GIG","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 20:45:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 06:15:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 15:29:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-24 17:59:00",format);
        flight.add(new Flights("AA","AA256",departTime1,7,"GIG",630,arriveTime1,"JFK","A380"));
        flight.add(new Flights("AA","AA256",departTime2,8,"JFK",150,arriveTime2,"ATL","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 20:45:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 06:15:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 15:29:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 17:59:00",format);
        flight.add(new Flights("AA","AA256",departTime1,9,"GIG",630,arriveTime1,"JFK","A380"));
        flight.add(new Flights("AA","AA256",departTime2,10,"JFK",150,arriveTime2,"ATL","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 20:45:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-26 06:15:00",format);
        departTime2 = LocalDateTime.parse("2017-09-26 15:29:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 17:59:00",format);
        flight.add(new Flights("AA","AA256",departTime1,11,"GIG",630,arriveTime1,"JFK","A380"));
        flight.add(new Flights("AA","AA256",departTime2,12,"JFK",150,arriveTime2,"ATL","A380"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 06:29:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-23 07:59:00",format);
        departTime2 = LocalDateTime.parse("2017-09-23 08:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-23 10:45:00",format);
        flight.add(new Flights("AA","AA303",departTime1,13,"LGA",150,arriveTime1,"ORD","757-200"));
        flight.add(new Flights("AA","AA303",departTime2,14,"ORD",250,arriveTime2,"LAX","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 06:29:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 07:59:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 08:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-24 10:45:00",format);
        flight.add(new Flights("AA","AA303",departTime1,15,"LGA",150,arriveTime1,"ORD","757-200"));
        flight.add(new Flights("AA","AA303",departTime2,16,"ORD",250,arriveTime2,"LAX","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 06:29:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 07:59:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 08:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 10:45:00",format);
        flight.add(new Flights("AA","AA303",departTime1,17,"LGA",150,arriveTime1,"ORD","757-200"));
        flight.add(new Flights("AA","AA303",departTime2,18,"ORD",250,arriveTime2,"LAX","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 18:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-23 20:40:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 09:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-24 11:40:00",format);
        flight.add(new Flights("AF","AF1439",departTime1,19,"VIE",125,arriveTime1,"CDG","767-400"));
        flight.add(new Flights("AF","AF1439",departTime2,20,"CDG",125,arriveTime2,"MAD","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 18:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 20:40:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 09:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 11:40:00",format);
        flight.add(new Flights("AF","AF1439",departTime1,21,"VIE",125,arriveTime1,"CDG","767-400"));
        flight.add(new Flights("AF","AF1439",departTime2,22,"CDG",125,arriveTime2,"MAD","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 18:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 20:40:00",format);
        departTime2 = LocalDateTime.parse("2017-09-26 09:35:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 11:40:00",format);
        flight.add(new Flights("AF","AF1439",departTime1,23,"VIE",125,arriveTime1,"CDG","767-400"));
        flight.add(new Flights("AF","AF1439",departTime2,24,"CDG",125,arriveTime2,"MAD","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 19:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-23 20:00:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 08:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-24 16:40:00",format);
        flight.add(new Flights("AF","AF5028",departTime1,25,"ORY",85,arriveTime1,"LHR","757-200"));
        flight.add(new Flights("AF","AF5028",departTime2,26,"LHR",400,arriveTime2,"DOH","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 19:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 20:00:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 08:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 16:40:00",format);
        flight.add(new Flights("AF","AF5028",departTime1,27,"ORY",85,arriveTime1,"LHR","757-200"));
        flight.add(new Flights("AF","AF5028",departTime2,28,"LHR",400,arriveTime2,"DOH","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 19:35:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 20:00:00",format);
        departTime2 = LocalDateTime.parse("2017-09-26 08:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 16:40:00",format);
        flight.add(new Flights("AF","AF5028",departTime1,29,"ORY",85,arriveTime1,"LHR","757-200"));
        flight.add(new Flights("AF","AF5028",departTime2,30,"LHR",400,arriveTime2,"DOH","757-200"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 23:40:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 16:10:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 19:20:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 00:30:00",format);
        flight.add(new Flights("AY","AY81",departTime1,31,"HEL",690,arriveTime1,"SIN","757-300"));
        flight.add(new Flights("AY","AY81",departTime2,32,"SIN",310,arriveTime2,"PER","757-300"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 23:40:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 16:10:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 19:20:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 00:30:00",format);
        flight.add(new Flights("AY","AY81",departTime1,33,"HEL",690,arriveTime1,"SIN","757-300"));
        flight.add(new Flights("AY","AY81",departTime2,34,"SIN",310,arriveTime2,"PER","757-300"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 23:40:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-26 16:10:00",format);
        departTime2 = LocalDateTime.parse("2017-09-26 19:20:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-27 00:30:00",format);
        flight.add(new Flights("AY","AY81",departTime1,35,"HEL",690,arriveTime1,"SIN","757-300"));
        flight.add(new Flights("AY","AY81",departTime2,36,"SIN",310,arriveTime2,"PER","757-300"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 06:30:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-23 08:05:00",format);
        departTime2 = LocalDateTime.parse("2017-09-23 10:25:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-23 16:25:00",format);
        flight.add(new Flights("DL","DL7253",departTime1,37,"BNE",95,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL7253",departTime2,38,"SYD",1200,arriveTime2,"ATL","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 06:30:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-24 08:05:00",format);
        departTime2 = LocalDateTime.parse("2017-09-24 10:25:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-24 16:25:00",format);
        flight.add(new Flights("DL","DL7253",departTime1,39,"BNE",95,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL7253",departTime2,40,"SYD",1200,arriveTime2,"ATL","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 06:30:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 08:05:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 10:25:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 16:25:00",format);
        flight.add(new Flights("DL","DL7253",departTime1,41,"BNE",95,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL7253",departTime2,42,"SYD",1200,arriveTime2,"ATL","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-23 22:50:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-25 07:50:00",format);
        departTime2 = LocalDateTime.parse("2017-09-25 11:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-25 12:30:00",format);
        flight.add(new Flights("DL","DL787",departTime1,43,"LAX",900,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL787",departTime2,44,"SYD",90,arriveTime2,"BNE","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-24 22:50:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-26 07:50:00",format);
        departTime2 = LocalDateTime.parse("2017-09-26 11:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-26 12:30:00",format);
        flight.add(new Flights("DL","DL787",departTime1,45,"LAX",900,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL787",departTime2,46,"SYD",90,arriveTime2,"BNE","767-400"));
        flights.add(flight);

        flight = new LinkedList<Flights>();
        departTime1 = LocalDateTime.parse("2017-09-25 22:50:00",format);
        arriveTime1 = LocalDateTime.parse("2017-09-27 07:50:00",format);
        departTime2 = LocalDateTime.parse("2017-09-27 11:00:00",format);
        arriveTime2 = LocalDateTime.parse("2017-09-27 12:30:00",format);
        flight.add(new Flights("DL","DL787",departTime1,47,"LAX",900,arriveTime1,"SYD","767-400"));
        flight.add(new Flights("DL","DL787",departTime2,48,"SYD",90,arriveTime2,"BNE","767-400"));
        flights.add(flight);
        */
        return flights;
    }
}
