//DAO Implementation for Reviews
package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.Review;
import seng3150.group3.beans.Tag;
import seng3150.group3.beans.TagName;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

@Repository
public class ReviewDAO implements ReviewDAOI {
    @Autowired
    private EntityManagerFactory emf;

    @Override
    //Pre: Systems wants to add review
    //Post: Adds given review to database
    public void addReview(Review review) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(review);
        em.flush();
        em.getTransaction().commit();
        em.close();
        return;
    }

    //Pre: Systems wants to add tag to review
    //Post: Tag added to database
    public void addTag(Tag tag) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(tag);
        em.flush();
        em.getTransaction().commit();
        em.close();
        return;
    }

    @Override
    //Pre: Systems wants to delete a review by reviewID
    //Post: Specified review is deleted
    public void deleteReview(int reviewID) {
        EntityManager em = emf.createEntityManager();
        Review review = em.find(Review.class, reviewID);
        em.getTransaction().begin();
        em.remove(review);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    //Pre: System wants to get a review by the id
    //Post: Returns a review with the primary key specified
    public Review getReview(int id) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Review> q = em.createQuery("SELECT r from Review r where r.reviewID = :reviewID", Review.class);
        q.setParameter("reviewID", id);
        Review review = q.getSingleResult();
        em.close();
        return review;
    }

    @Override
    //Pre: System wants all reviews for a fight
    //Post: List of reviews for that flight returned
    public List<Review> getFlightReviews(int flightID) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Review> q = em.createQuery("SELECT r from Review r where r.flightID = :flightID", Review.class);
        q.setParameter("flightID", flightID);
        List<Review> reviews = q.getResultList();
        em.close();
        return reviews;
    }

    @Override
    //Pre: system wants all tags for review with reviewID
    //Post: Returns list of tags for that specified review
    public List<Tag> getReviewTags(int reviewID) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Tag> q = em.createQuery("SELECT t from Tag t where t.reviewID = :reviewID", Tag.class);
        q.setParameter("reviewID", reviewID);
        List<Tag> tags = q.getResultList();
        em.close();
        return tags;
    }

    /*@Override
    //Pre: System wants all reviews
    //Post: Returns list of all reviews
    public List<Review> getAllReviews() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Review> q = em.createQuery("Select r from Review r", Review.class);
        List<Review> reviews = q.getResultList();
        em.close();
        return reviews;
    }*/

    /*@Override
    //Pre: System wants all tags
    //Post: returns list with tags for every review
    public List<Tag> getAllTags() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Tag> q = em.createQuery("SELECT t from Tag t", Tag.class);
        List<Tag> tags = q.getResultList();
        em.close();
        return tags;
    }*/

    @Override
    //Pre: system wants all tag categories
    //Post: Returns list with all tag categories
    public List<TagName> getAllTagNames() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<TagName> q = em.createQuery("SELECT t from TagName t", TagName.class);
        List<TagName> tagNames = q.getResultList();
        em.close();
        return tagNames;
    }

    @Override
    //Post: System wants to add a new tag category
    //Post: New tag category persisted to database
    public void addTagName(TagName newTag) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(newTag);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }

    /*@Override
    //This is never used?
    public TagName findTagName(String tagName) {
        EntityManager em = emf.createEntityManager();
        return null;
    }*/

    @Override
    //Pre: System wants to delete a tag category
    //Post: Specified tag category deleted from system
    public void deleteTagName(String deleteTag) {
        EntityManager em = emf.createEntityManager();
        TagName tag = em.find(TagName.class, deleteTag);
        em.getTransaction().begin();
        em.remove(tag);
        em.flush();
        em.getTransaction().commit();
        em.close();
    }
}
