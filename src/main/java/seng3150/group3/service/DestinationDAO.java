package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.Destinations;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class DestinationDAO implements DestinationDAOI {
    @Autowired
    EntityManagerFactory emf;

    public List<Destinations> getAllDestinations() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Destinations> q = em.createQuery("FROM Destinations d", Destinations.class);
        List<Destinations> result = q.getResultList();
        em.close();
        return result;
    }

    /*public Destinations getDestinations(String destinationCode) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Destinations> q = em.createQuery("FROM Destinations d WHERE d.destinationCode = :destinationCode", Destinations.class);
        q.setParameter("destinationCode", destinationCode);
        return q.getSingleResult();
    }*/
}
