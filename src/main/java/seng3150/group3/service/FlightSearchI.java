package seng3150.group3.service;

import seng3150.group3.beans.Flights;

import java.util.Date;
import java.util.List;

public interface FlightSearchI {
    void buildFlightPathGraph();
    List<List<Flights>> search(String departureCode, String arrivalCode, Date departureTime, int passengers, String flightClass);
}
