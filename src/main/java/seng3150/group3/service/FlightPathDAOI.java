package seng3150.group3.service;

import seng3150.group3.beans.FlightPath;

import java.util.List;

public interface FlightPathDAOI {
    List<FlightPath> getFlightPaths();
}
