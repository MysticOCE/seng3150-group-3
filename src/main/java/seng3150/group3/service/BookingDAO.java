package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.Booking;
import seng3150.group3.beans.BookingOrder;
import seng3150.group3.beans.PurchaseHistory;
import seng3150.group3.beans.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Date;

@Repository
//Implementation of BookingDAO
public class BookingDAO implements BookingDAOI {
    @Autowired
    private EntityManagerFactory emf;

    @Override
    //Pre: System is adding a booking to database and has provided details
    //Post: Booking is persisted to database
    public void addBooking(int addbookingId, int userId, int addflightId, String addclasscode) {
        EntityManager em = emf.createEntityManager();
        Booking booking = new Booking(addbookingId, addflightId, addclasscode);
        em.getTransaction().begin();
        em.merge(booking);
        //Gets the latest booking in the database.
        TypedQuery<Booking> q = em.createQuery("SELECT b from Booking b WHERE b.bookingID=(SELECT max(bookingID) from Booking)", Booking.class);
        List<Booking> bookings = q.getResultList();
        Booking bookingLatest = bookings.get(bookings.size()-1);
        BookingOrder bookingOrder = new BookingOrder(1,userId,bookingLatest.getBookingID());
        em.merge(bookingOrder);
        em.getTransaction().commit();
        em.close();
        return;
    }

    @Override
    //Pre: System is deleting a booking
    //Post: a booking is removed from the database
    public void deleteBooking(int bookingID) {
        //Note that bookingID is the ID of the booking order, NOT the id of the booking!
        //Note: Does not check for user yet! So any user can delete any booking!!!
        EntityManager em2 = emf.createEntityManager();
        BookingOrder bookingOrder = em2.find(BookingOrder.class, bookingID);//Check if this is null?
        int getBookingId = bookingOrder.getBookingID();
        em2.getTransaction().begin();
        em2.remove(bookingOrder);
        em2.getTransaction().commit();
        em2.close();

        EntityManager em = emf.createEntityManager();
        Booking booking = em.find(Booking.class, getBookingId);
        em.getTransaction().begin();
        em.remove(booking);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    //Pre: System requests bookings made by a certain user
    //Post: returns bookings made by a specific user
    public List<BookingOrder> getUserBookingOrder(int userID) {

        EntityManager em = emf.createEntityManager();
        TypedQuery<BookingOrder> q = em.createQuery("SELECT b from BookingOrder b WHERE b.userID = :userID", BookingOrder.class);
        q.setParameter("userID", userID);
        List<BookingOrder> bookingOrder = q.getResultList();
        em.close();
        return bookingOrder;
    }

    @Override
    //Pre: System wants all bookings for a given user
    //Post: returns a list of all booking made by the specified user
    public List<Booking> getAllUserBookingOrders(int userID) {   //Retrieves all bookingOrders by a user as bookings

        EntityManager em = emf.createEntityManager();
        TypedQuery<Booking> q = em.createQuery("SELECT b FROM Booking b, BookingOrder bo WHERE (b.bookingID = bo.bookingID AND bo.userID = :userID)", Booking.class);
        q.setParameter("userID", userID);
        List<Booking> bookings = q.getResultList();
        em.close();
        return bookings;
    }

    @Override
    //Pre: System wants bookings as purchase history
    //Post: for the specified user, returns bookings as purchase history
    public List<Booking> getAsBookingUserPurchaseHistory(int userID) {   //Retrieves all purchaseHistory by a user as bookings

        EntityManager em = emf.createEntityManager();
        TypedQuery<Booking> q = em.createQuery("SELECT b FROM Booking b, PurchaseHistory bo WHERE (b.bookingID = bo.bookingID AND bo.userID = :userID)", Booking.class);
        q.setParameter("userID", userID);
        List<Booking> bookings = q.getResultList();
        em.close();
        return bookings;
    }

    @Override
    //Pre: System wants purchase history for a user
    //Post: Returns list of purchase history for specified user
    public List<PurchaseHistory> getUserPurchaseHistory(int userID) {   //Retrieves all purchaseHistory by a user as purchaseHistory

        EntityManager em = emf.createEntityManager();
        TypedQuery<PurchaseHistory> q = em.createQuery("SELECT b from PurchaseHistory b WHERE b.userID = :userID", PurchaseHistory.class);
        q.setParameter("userID", userID);
        List<PurchaseHistory> PurchaseHistory = q.getResultList();
        em.close();
        return PurchaseHistory;
    }

    @Override
    //Pre: Bookings have been confirmed by a user
    //Post: Bookings are moved to purchase history for that user
    public void setUserPurchaseHistory(int userID) {//Sets the booking orders to become purchase history for the user.
        //ToDo: Return null if no userID's found
        List<Booking> bookingList = getAllUserBookingOrders(userID);
        //Adds all bookingID's to new instances of purchaseHistory;
        for(int i = 0; i < bookingList.size(); i++) {
            Date thisDate = new Date();
            PurchaseHistory newPurchaseHistory = new PurchaseHistory(1,userID, bookingList.get(i).getBookingID(), thisDate);
            EntityManager em2 = emf.createEntityManager();
            em2.getTransaction().begin();
            em2.merge(newPurchaseHistory);
            em2.getTransaction().commit();
            em2.close();
        }
        //Removes bookingID's from bookingOrder
        for(int i = 0; i < bookingList.size(); i++) {
            EntityManager em2 = emf.createEntityManager();
            TypedQuery<BookingOrder> q = em2.createQuery("SELECT b from BookingOrder b WHERE b.bookingID = :bookingID", BookingOrder.class);
            q.setParameter("bookingID", bookingList.get(i).getBookingID());
            BookingOrder bookingOrder = q.getSingleResult();//Check if this is null?
            if(bookingOrder != null) {
                em2.getTransaction().begin();
                em2.remove(bookingOrder);
                em2.getTransaction().commit();
                em2.close();
            }
        }
    }
}
