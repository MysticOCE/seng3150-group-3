package seng3150.group3.service;

import seng3150.group3.beans.Review;
import seng3150.group3.beans.Tag;
import seng3150.group3.beans.TagName;

import java.util.List;

//interface for ReviewDAO
public interface ReviewDAOI {

    void addReview(Review review);
    void addTag(Tag tag);
    void deleteReview(int reviewID);

    Review getReview(int id);

    List<Review> getFlightReviews(int flightID);

    List<Tag> getReviewTags(int reviewID);

    //List<Review> getAllReviews();

   // List<Tag> getAllTags();

    List<TagName> getAllTagNames();
    void addTagName(TagName newTag);
    //TagName findTagName(String tagName);
    void deleteTagName(String deleteTag);

}
