package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.Flights;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class FlightDAO implements FlightDAOI {
    @Autowired
    private EntityManagerFactory emf;
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public Flights getFlight(String airlineCode, String flightNumber, Date departureTime) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Flights> q = em.createQuery("FROM Flights f where f.AirlineCode = :airlineCode and f.FlightNumber = :flightNumber and f.DepartureTime = :departureTime", Flights.class);
        q.setParameter("airlineCode", airlineCode);
        q.setParameter("flightNumber", flightNumber);
        q.setParameter("departureTime", departureTime);
        Flights result = q.getSingleResult();
        em.close();
        return result;
    }

    public Flights getFlight(int flightId) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Flights> q = em.createQuery("FROM Flights f WHERE f.id = :flightId", Flights.class);
        q.setParameter("flightId", flightId);
        Flights result = q.getSingleResult();
        em.close();
        return result;
    }

    public List<Flights> getFlightsForSearch(String departureCode, String destinationCode, Date departureTime1, Date departureTime2) {
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        EntityManager em = emf.createEntityManager();
        //TypedQuery<Flights> q = em.createQuery("SELECT f FROM Flights f where f.departureCode.destinationCode = :departureCode and f.destinationCode.destinationCode = :destinationCode and f.key.departureTime = :departureTime", Flights.class);
        TypedQuery<Flights> q = em.createQuery("FROM Flights f where f.departureCode.destinationCode = :departureCode and f.destinationCode.destinationCode = :destinationCode and f.key.departureTime BETWEEN :departureTime1 AND :departureTime2 order by f.key.departureTime", Flights.class);
        q.setParameter("departureCode", departureCode);
        q.setParameter("destinationCode", destinationCode);
        //q.setParameter("departureTime", dtf.format(departureTime));

        //q.setParameter("departureTime", departureTime1);
        q.setParameter("departureTime1", departureTime1);
        q.setParameter("departureTime2", departureTime2);

        List<Flights> results = q.getResultList();
        em.close();
        return results;
    }

    //public List<Flights> getNthLegFlightsForSearch(String airlineCode, String departureCode, String destinationCode, Date departureTime1, Date departureTime2) {
    public List<Flights> getNthLegFlightsForSearch(String departureCode, String destinationCode, Date departureTime1, Date departureTime2) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Flights> q = em.createQuery("SELECT f FROM Flights f where f.departureCode.destinationCode = :departureCode and f.destinationCode.destinationCode = :destinationCode and f.key.departureTime BETWEEN :departureTime1 and :departureTime2 order by f.key.departureTime", Flights.class);
        /*TypedQuery<Flights> q = em.createQuery("SELECT f FROM Flights f where f.airline.airlineCode = :airlineCode and f.departureCode.destinationCode = :departureCode and f.destinationCode.destinationCode = :destinationCode and f.key.departureTime BETWEEN :departureTime1 and :departureTime2 order by f.key.departureTime", Flights.class);
        q.setParameter("airlineCode", airlineCode);*/
        q.setParameter("departureCode", departureCode);
        q.setParameter("destinationCode", destinationCode);
        //q.setParameter("departureTime", dtf.format(departureTime));

        //q.setParameter("departureTime", departureTime1);
        q.setParameter("departureTime1", departureTime1);
        q.setParameter("departureTime2", departureTime2);

        List<Flights> results = q.getResultList();
        em.close();
        return results;
    }

    /*public List<Flights> getAllFlights() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Flights> q = em.createQuery("FROM Flights", Flights.class);

        return q.getResultList();
    }*/
}
