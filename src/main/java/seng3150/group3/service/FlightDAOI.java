package seng3150.group3.service;

import seng3150.group3.beans.Flights;

import java.util.Date;
import java.util.List;

public interface FlightDAOI {
    Flights getFlight(String airlineCode, String flightNumber, Date DepartureTime);
    Flights getFlight(int id);
    List<Flights> getFlightsForSearch(String departureCode, String destinationCode, Date departureTime1, Date departureTime2);
    //List<Flights> getNthLegFlightsForSearch(String airlineCode, String departureCode, String destinationCode, Date departureTime1, Date departureTime2);
    List<Flights> getNthLegFlightsForSearch(String departureCode, String destinationCode, Date departureTime1, Date departureTime2);
    //  List<Flights> getAllFlights();
}
