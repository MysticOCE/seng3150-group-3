package seng3150.group3.service;

import seng3150.group3.beans.Destinations;
import java.util.List;

public interface DestinationDAOI {
    List<Destinations> getAllDestinations();
    //Destinations getDestinations(String destinationCode);
}
