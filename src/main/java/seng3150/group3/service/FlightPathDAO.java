package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.FlightPath;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class FlightPathDAO implements FlightPathDAOI {
    @Autowired
    private EntityManagerFactory emf;

    public List<FlightPath> getFlightPaths() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<FlightPath> q = em.createQuery("SELECT fp FROM FlightPath fp", FlightPath.class);
        List<FlightPath> tempList = q.getResultList();
        em.close();
        return tempList;
    }
    /*
     EntityManager em = emf.createEntityManager();
        TypedQuery<Flights> q = em.createQuery("FROM Flights f where f.AirlineCode = :airlineCode and f.FlightNumber = :flightNumber and f.DepartureTime = :departureTime", Flights.class);
        q.setParameter("airlineCode", airlineCode);
        q.setParameter("flightNumber", flightNumber);
        q.setParameter("departureTime", format.format(departureTime));
        em.close();
        return q.getSingleResult();
     */
}
