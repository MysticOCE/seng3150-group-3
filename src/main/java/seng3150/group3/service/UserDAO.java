package seng3150.group3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import seng3150.group3.beans.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

//DAO class for the User object
@Repository
public class UserDAO implements UserDAOI {
    @Autowired
    private EntityManagerFactory emf;

    //Pre: System wants to register a new User
    //Post: Adds a new user to the database
    public void addUser(User user) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(user);         //Needed to change to merge as persist wasn't working.
        //em.flush();
        em.getTransaction().commit();
        em.close();
        return;
    }

    @Override
    //Pre: System want to get user details by userID
    //Post: Returns a user object defined by that id
    public User getUser(int userID) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT u from User u where u.userID = :userID", User.class);
        q.setParameter("userID", userID);
        User res = q.getSingleResult();
        em.close();
        return res;
    }

    @Override
    //Pre: System wants to get a user by their email
    //Post: System returns a user based off that email
    public User verifyUser(String email) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT u from User u where u.email = :email", User.class);
        q.setParameter("email", email);
        try {
            User res = q.getSingleResult();
            em.close();
            return res;
        }
        catch(javax.persistence.NoResultException e) {
            em.close();
            return null;
        }
    }

    @Override
    public boolean checkUser(String email, String password) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT u from User u where u.email = :email", User.class);
        q.setParameter("email", email);
        try {
            User res = q.getSingleResult();
            em.close();
            if (password.equalsIgnoreCase(res.getPassword())) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (javax.persistence.NoResultException e) {
            em.close();
            return false;
        }
    }

    @Override
    //Pre: System wants all users
    //Post: Returns list of all users
    //I don't think this is ever actually used, it was there for testing hibernate
    public List<User> getAllUsers() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT u FROM User u", User.class);
        List<User> res = q.getResultList();
        em.close();
        return res;
    }
}
