package seng3150.group3.service;

import seng3150.group3.beans.User;

import java.util.List;

public interface UserDAOI {

    void addUser(User user);

    User getUser(int id);

    User verifyUser(String email);

    boolean checkUser(String email, String password);

    List<User> getAllUsers();
}
