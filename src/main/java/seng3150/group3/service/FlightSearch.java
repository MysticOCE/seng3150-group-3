package seng3150.group3.service;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.KShortestSimplePaths;
import org.jgrapht.graph.DirectedMultigraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seng3150.group3.beans.Destinations;
import seng3150.group3.beans.FlightPath;
import seng3150.group3.beans.Flights;

import javax.servlet.ServletContext;
import java.util.*;
import java.util.function.Supplier;

@Service
public class FlightSearch implements FlightSearchI {
    private DirectedMultigraph<String, String> flightPaths = null;//String->destinationCode, String->the edge
    @Autowired
    private DestinationDAOI destinations;

    @Autowired
    private FlightPathDAOI fps;

    @Autowired
    ServletContext lf;

    @Autowired
    private FlightDAOI flights;

    /**
     * Build flight path graph for searching flight path
     */
    public void buildFlightPathGraph() {
        if(flightPaths != null) {
            return;
        }
        Supplier<String> vs = null;
        Supplier<String> es = null;
        DirectedMultigraph<String, String> flightPaths = new DirectedMultigraph<String, String>(vs, es, true);

        /*
        Build vertices
         */
        List<Destinations> allDestinations  = destinations.getAllDestinations();
        for(Destinations d : allDestinations) {
            lf.log("Add vertex: " + d.getDestinationCode());
            flightPaths.addVertex(d.getDestinationCode());
        }

        /*
        Build edges
         */
        //Get list of Departure to Arrival edges with duration as weight
        List<FlightPath> fp = fps.getFlightPaths();
        int edge = 0;
        for(FlightPath flightP : fp) {
            String edgeStr = edge+"";
            flightPaths.addEdge(flightP.getDepartureCode(), flightP.getDestinationCode(), edgeStr);
            flightPaths.setEdgeWeight(edgeStr, flightP.getDuration());
            edge++;
        }

        this.flightPaths = flightPaths;
        for(FlightPath f : fp) {
            lf.log("Flight path: "+f.getDepartureCode()+" -> "+f.getDestinationCode() + " : " + f.getDuration());
        }
        lf.log("Graph build complete");
    }

    @Override
    public List<List<Flights>> search(String departureCode, String arrivalCode, Date departureTime, int passengers, String flightClass) {
        buildFlightPathGraph();//In case graph not built

        //GraphPath<String, String> shortestPath = DijkstraShortestPath.findPathBetween(flightPaths, departureCode, arrivalCode);//get the shortest path in the graph from the two destinations
        List<GraphPath<String, String>> shortestPaths = new KShortestSimplePaths<>(flightPaths).getPaths(departureCode, arrivalCode, 3);//This is to get k shortest paths (in case more flights need to be returned)
        GraphPath<String, String> shortestPath = shortestPaths.get(0);

        if(shortestPath == null)
            return null;

        List<String> shortestPathList = shortestPath.getVertexList();//the list of vertices to get shortest path
        List<List<Flights>> flightList = new LinkedList<List<Flights>>();//the end result of searching --> return this

        for(String dest : shortestPathList) {//Print shortest path list
            lf.log("destination airport: " + dest);
        }

        /*
        Get flights for first leg of flight
         */
        List<Flights> departureSet = flights.getFlightsForSearch(departureCode, shortestPathList.get(1), moveTimeByDays(departureTime, -1), moveTimeByDays(departureTime, 2));//get first set of flights

        lf.log("shortest path size: "+shortestPathList.size());

        /*
        If there are only two destinations in the path list --> no flight legs, so can just return List of single Flights
         */
        if(shortestPathList.size() <= 2) {
            for(Flights flight : departureSet) {
                LinkedList<Flights> tempFlightList = new LinkedList<>();
                tempFlightList.add(flight);
                flightList.add(tempFlightList);
            }
            return flightList;
        }

        /*
        For multileg flights:
         */
        for(Flights departureFlight : departureSet) {//For each top level result
            LinkedList<Flights> tempFlight = new LinkedList<>();
            tempFlight.add(departureFlight);//This is the first flight

            Iterator<String> spIt = shortestPathList.iterator();
            lf.log("path list size: "+shortestPathList.size());
            lf.log("throw away: "+spIt.next());
            lf.log("throw away: "+spIt.next());//This is to throw away first result - already taken from first level results
            String nextDestination;
            boolean isGoodFlight = true;
            while(spIt.hasNext()) {
                nextDestination = spIt.next();
                //while((nextDestination = spIt.next()) != null) {//for each vertex from shortest path [1]->[end]
                Flights lastFlight = tempFlight.getLast();
                Date legMinDepartureTime = moveTimeByMinutes(lastFlight.getArrivalTime(), 30);//Search for time by at least 30 mins past arrival time
                //Date legMaxDepartureTime = moveTimeByMinutes(lastFlight.getArrivalTime(), 500);
                Date legMaxDepartureTime = moveTimeByDays(lastFlight.getArrivalTime(), 1);
                String airline = lastFlight.getAirlineCode();
                String nextDepartureCode = lastFlight.getDestinationCode().getDestinationCode();

                List<Flights> nextFlightList = flights.getNthLegFlightsForSearch(
                        nextDepartureCode,
                        nextDestination,
                        legMinDepartureTime,
                        legMaxDepartureTime);

                if(nextFlightList.size() == 0) {//If there are no next flights
                    lf.log(airline+": "+nextDepartureCode+"->"+nextDestination+" @"+legMaxDepartureTime+" to "+legMinDepartureTime+" bad. Breaking");
                    isGoodFlight = false;
                    break;//Break out of while loop before consuming all nextDestinations
                }
                lf.log(airline+": "+nextDepartureCode+"->"+nextDestination+" @"+legMaxDepartureTime+" to "+legMinDepartureTime+" good");
                Flights nextFlight = nextFlightList.get(0);
                tempFlight.add(nextFlight);
            }
            //if(!spIt.hasNext()) {//If the loop stopped before completing
            if(isGoodFlight) {//If flight is good
                flightList.add(tempFlight);//Add flight to list
            }
        }

        /*if(shortestPathList.get(1).equals(arrivalCode)) {
            for(Flights f : departureSet) {
                LinkedList<Flights> fList = new LinkedList<>();
                fList.add(f);
                flightList.add(fList);
            }
            return flightList;
        }*/
        return flightList;
    }

    /**
     * Increments Date by specified minutes
     * @param baseDate the base Date to increment on
     * @param timeInMinutes time in minutes to increment by
     * @return incremented Date
     */
    private Date moveTimeByMinutes(Date baseDate, int timeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(baseDate);
        cal.add(Calendar.MINUTE, timeInMinutes);
        return cal.getTime();
    }

    /**
     * Increments Date by specified days
     * @param baseDate the base Date to increment on
     * @param timeInDays time in minutes to increment by
     * @return incremented Date
     */
    private Date moveTimeByDays(Date baseDate, int timeInDays) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(baseDate);
        cal.add(Calendar.DAY_OF_MONTH, timeInDays);
        return cal.getTime();
    }
}
