package seng3150.group3.service;

import seng3150.group3.beans.Booking;
import seng3150.group3.beans.BookingOrder;
import seng3150.group3.beans.PurchaseHistory;

import java.util.List;
//interface for BookingDAO
public interface BookingDAOI {
    void addBooking(int bookingId, int userId, int flightId, String classcode);
    public List<BookingOrder> getUserBookingOrder(int userID);
    void deleteBooking(int bookingID);
    public List<Booking> getAllUserBookingOrders(int userID);
    public void setUserPurchaseHistory(int userID);
    public List<Booking> getAsBookingUserPurchaseHistory(int userID);
    public List<PurchaseHistory> getUserPurchaseHistory(int userID);
}
