//Controller object for register page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.beans.User;
import seng3150.group3.service.UserDAOI;

import java.util.Map;

@Controller
@RequestMapping("/register")
public class RegisterController {
    @Autowired
    UserDAOI userService;

    @GetMapping
    //Pre: User navigated to the register page
    //PosT: User is returned a form where they can register
    public ModelAndView register() {
        User user = new User();
        ModelAndView view = new ModelAndView("/register");
        view.addObject("registerForm", user);
        return view;
    }

    @PostMapping
    //Pre: User has filled in register form and submitted it
    //Post: User is added to database
    public ModelAndView registerSubmit(@ModelAttribute("registerForm") User u, Map<String, Object> model) {
        ModelAndView view = new ModelAndView("/register");
        userService.addUser(u);
        return view;
    }
}
