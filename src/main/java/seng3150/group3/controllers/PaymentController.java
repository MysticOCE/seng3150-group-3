//Controller object for payment page.
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.mail.javamail.*;
import org.springframework.mail.SimpleMailMessage;
import seng3150.group3.beans.User;
import seng3150.group3.beans.Booking;
import seng3150.group3.beans.BookingOrder;
import seng3150.group3.beans.PurchaseHistory;
import seng3150.group3.service.UserDAOI;
import seng3150.group3.service.BookingDAOI;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.beans.Flights;
import seng3150.group3.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    UserDAOI userService;

    @Autowired
    BookingDAOI bookingService;

    @Autowired
    FlightDAOI flightService;

    @GetMapping
    public ModelAndView payment(@RequestParam(name="email", required = false) String email) {
        ModelAndView view = new ModelAndView("secured/payment");

        //Retrieves user ID from email (if present)
        if(email != null) {
            User getUser = userService.verifyUser(email);
            if(getUser != null) {
                List<BookingOrder> bookingsOrderList = bookingService.getUserBookingOrder(getUser.getUserID()); //Replace 2 with required userid here!
                view.addObject("bookingsOrderList", bookingsOrderList);
                List<Booking> bookingsList = bookingService.getAllUserBookingOrders(getUser.getUserID());
                view.addObject("bookingsList", bookingsList);
                List<PurchaseHistory> purchaseHistory = bookingService.getUserPurchaseHistory(getUser.getUserID());
                view.addObject("purchaseHistory", purchaseHistory);

                //Creates list of flights
                List<Flights> flightsList = new ArrayList<>();
                for(int i = 0; i < bookingsList.size(); i++) {
                    //Only adds unique flights
                    boolean add = true;
                    for(int i2 = 0; i2 < flightsList.size(); i2++)
                    {
                        if(flightsList.get(i2).getId() == bookingsList.get(i).getFlightID()) {
                            add = false;
                        }
                    }
                    if(add == true) {
                        flightsList.add(flightService.getFlight(bookingsList.get(i).getFlightID()));
                    }

                }
                view.addObject("flights", Utility.singleFlightInfos(flightsList));
				
				//Creates a list of flights that is passed on to the purchase summary page.
				//List<Flights> sendList = new ArrayList<>(); 
				String sendList = "";
				//List<String> sendList = new ArrayList<>();
				for(int i = 0; i < bookingsOrderList.size(); i++){
					for(int i2 = 0; i2 < bookingsList.size(); i2++){
						if(bookingsOrderList.get(i).getBookingID() == bookingsList.get(i2).getBookingID()){
							for(int i3 = 0; i3 < flightsList.size(); i3++){
								if(flightsList.get(i3).getId() == bookingsList.get(i2).getFlightID()){
									//sendList.add(flightsList.get(i3));
									sendList += Integer.toString(flightsList.get(i3).getId()) + ",";
									//sendList.add(Integer.toString(flightsList.get(i3).getId()));
								}
							}
						}
					}
				}
                //view.addObject("sendflights", Utility.singleFlightInfos(sendList));
				view.addObject("sendflights", sendList);
            }
        }
        return view;
    }
}
