//Controller for deleting tags.
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import seng3150.group3.service.ReviewDAOI;

@Controller
@RequestMapping("/deleteTag")
public class TagController {
    @Autowired
    ReviewDAOI reviewService;

    @GetMapping
    //Pre: Admin has clicked "delete" on a tag category
    //Post: Tag category is removed from database.
    public String deleteTag(@RequestParam(value = "tag", required = true) String deleteTag) {
        reviewService.deleteTagName(deleteTag);
        return "redirect:/";
    }
}
