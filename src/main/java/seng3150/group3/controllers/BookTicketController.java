package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.Utility;
import seng3150.group3.beans.FlightLeg;
import seng3150.group3.beans.User;
import seng3150.group3.service.UserDAOI;
import seng3150.group3.beans.Flights;
import seng3150.group3.service.BookingDAOI;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/book-ticket")
//Controller object for the book ticket page
public class BookTicketController {
    @Autowired
    BookingDAOI bookingService;

    @Autowired
    UserDAOI userService;

    @Autowired
    Utility util;

    @GetMapping
    @PostMapping
    public ModelAndView bookTicket(@RequestParam(name="id", required = true) String id) {
        ModelAndView view = new ModelAndView("secured/book-ticket");
        User user = userService.getUser(2);
        view.addObject("user", user);
        LinkedList<Flights> multiFlight = util.getMultiFlight(id);

        List<FlightLeg> flightLegs = Utility.getLegs(multiFlight);

        view.addObject("legs", flightLegs);
        view.addObject("id", id);
        view.addObject("totalDuration", Utility.totalDuration(multiFlight));
        return view;
    }
}
