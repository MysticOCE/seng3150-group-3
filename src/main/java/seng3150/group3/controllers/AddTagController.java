package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import seng3150.group3.beans.Review;
import seng3150.group3.beans.Tag;
import seng3150.group3.beans.TagName;
import seng3150.group3.service.ReviewDAOI;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/add-tags")
//Controller for adding tags to the database.
public class AddTagController {
    @Autowired
    ReviewDAOI reviewService;

    @GetMapping
    //Pre: Review is made and user is sent here
    //Post: Returns a page where you can add tags for the review you just made
    public ModelAndView addTag(@RequestParam(value="review", required = true) int reviewID) {
        ModelAndView view = new ModelAndView("secured/add-tags");
        //fetches tags that exist for review we are adding tags for
        List<Tag> tags = reviewService.getReviewTags(reviewID);
        //fetches the actual review we are adding tags for
        Review review = reviewService.getReview(reviewID);
        //Gets all the different tag categories
        List<TagName> tagNames = reviewService.getAllTagNames();
        //sets up possible scores for the tags.
        List<Integer> possibleScores = new ArrayList<Integer>();
        possibleScores.add(1);
        possibleScores.add(2);
        possibleScores.add(3);
        possibleScores.add(4);
        possibleScores.add(5);
        //gets tag categories and converts them to strings for easy usage in JSP
        List<String> names = new ArrayList<String>();
        for(TagName tagName: tagNames) {
            names.add(tagName.getTagName());
        }
        //form backing bean
        Tag tag = new Tag();
        //adding data to view
        view.addObject("review", review);
        view.addObject("tags",tags);
        view.addObject("names", names);
        view.addObject("tag", tag);
        view.addObject("scores", possibleScores);
        return view;
    }

    @PostMapping
    //Pre: User has submitted a tag with a score
    //Post: adds tag to database and then returns them to the tag adding page.
    public String submitTag(@ModelAttribute("tag") Tag tag, BindingResult result, ModelMap model, RedirectAttributes redir) {
        reviewService.addTag(tag);
        String reviewID = String.valueOf(tag.getReviewID());
        redir.addAttribute("review", reviewID);
        return "redirect:add-tags";
    }
}
