package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.mail.javamail.*;
import org.springframework.mail.SimpleMailMessage;
import seng3150.group3.beans.User;
import seng3150.group3.beans.Booking;
import seng3150.group3.beans.BookingOrder;
import seng3150.group3.beans.PurchaseHistory;
import seng3150.group3.service.UserDAOI;
import seng3150.group3.service.BookingDAOI;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.beans.Flights;
import seng3150.group3.Utility;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping("/bookings")
//Controller object for the bookings page
public class BookingsController {
    @Autowired
    UserDAOI userService;

    @Autowired
    BookingDAOI bookingService;

    @Autowired
    FlightDAOI flightService;

    @Autowired
    Utility util;

    @PostMapping
    //Note that this is never used.
    public ModelAndView bookings() {
        ModelAndView view = new ModelAndView("secured/bookings");
        /*
        List<Booking> bookingsList = bookingService.getUserBookings(2); //Replace 2 with required userid here!
        view.addObject("bookingsList", bookingsList);
        */
        //addBooking();
        return view;
    }

    @GetMapping
    //Pre: User has clicked book button
    //Post: Adds a booking to the booking database and sends them back to the booking page
    public ModelAndView bookings(@RequestParam(name="add", required = false) String addFlightID, @RequestParam(name="delete", required = false) String deleteID, @RequestParam(name="class", required = false) String inputClassCode, @RequestParam(name="passengers", required = false) String inputPassengers, @RequestParam(name="email", required = false) String email) {
        //TODO: Make sure it can't add something that the user shouldn't have access to!
        //TODO: Add to flight booking link
        if((addFlightID != null) && (addFlightID!="null")) {

            LinkedList<Flights> flights = util.getMultiFlight(addFlightID);
            for (Flights flight : flights) {
                int passengers = Integer.parseInt(inputPassengers);
                // Checks passengers first to make sure they're not adding e.g. 99999 passengers
                if (passengers > 0 && passengers < 6) {
                    addBooking(flight.getId(), inputClassCode, email);
                }
            }
        }
        //TODO: Make sure it can't delete something that the user has no access to!
        if(deleteID != null) {
            int deleteIDint = Integer.parseInt(deleteID);
            deleteBooking(deleteIDint);
        }
        ModelAndView view = new ModelAndView("secured/bookings");

        //Retrieves user ID from email (if present)
        if(email != null) {
            User getUser = userService.verifyUser(email);
            if(getUser != null) {
                List<BookingOrder> bookingsOrderList = bookingService.getUserBookingOrder(getUser.getUserID()); //Replace 2 with required userid here!
                view.addObject("bookingsOrderList", bookingsOrderList);
                List<Booking> bookingsList = bookingService.getAllUserBookingOrders(getUser.getUserID());
                view.addObject("bookingsList", bookingsList);
                List<PurchaseHistory> purchaseHistory = bookingService.getUserPurchaseHistory(getUser.getUserID());
                view.addObject("purchaseHistory", purchaseHistory);

                //Creates list of flights
                List<Flights> flightsList = new ArrayList<>();
                for(int i = 0; i < bookingsList.size(); i++) {
                    //Only adds unique flights
                    boolean add = true;
                    for(int i2 = 0; i2 < flightsList.size(); i2++)
                    {
                        if(flightsList.get(i2).getId() == bookingsList.get(i).getFlightID()) {
                            add = false;
                        }
                    }
                    if(add == true) {
                        flightsList.add(flightService.getFlight(bookingsList.get(i).getFlightID()));
                    }

                }
                // view.addObject("flightsList", flightsList);
                view.addObject("email", email);
                view.addObject("flights", Utility.singleFlightInfos(flightsList));

            }
        }
        //setPurchaseHistory(2);
        //Selects only bookings that are in bookingOrder
        //sendTestMail("");     Set this to an email that you want to send to.
        return view;
    }

    //Pre: called to add a booking to the database
    //Post: adds a booking to the database
    public void addBooking(int flightID, String classCode, String email) {
        //Adds booking
        User thisUser = returnUserFromEmail(email);
        bookingService.addBooking(1,thisUser.getUserID(),flightID,classCode);
    }

    public User returnUserFromEmail(String email) {
        //Checks user table based on email.
        //If there is a userID associated with that email, sets the bookings to that userID
        //If there is not, creates a new userID with that email, and sets the bookings to that userID.
        User getUser = userService.verifyUser(email);
        if(getUser == null) {
            //Gets list of all users
            List<User> allUsers = userService.getAllUsers();    //Gets the id of the latest user to pass to new user ID.
            //Creates a new user.
            User newUser = new User(allUsers.get(allUsers.size()-1).getUserID()+1, "3Test3", "", email, "000000", "");
            userService.addUser(newUser);
            getUser = userService.verifyUser(email);
            //TODO: set user role?
        }
        return getUser;
    }

    //Pre: called to remove a booking
    //Post: removes a booking
    public void deleteBooking(int deleteID) {
        //Note that bookingOrderID is the ID of the booking order, NOT the id of the booking!
        bookingService.deleteBooking(deleteID);
    }

    //Pre: called to set a user's purchase history
    //Post: set's a user's purchase history
    public void setPurchaseHistory(int userID) {
        bookingService.setUserPurchaseHistory(userID);  //Sets user's purchase history here!
    }

    //Pre: called to send a mail to someone
    //Post: Sends an email to someone about their booking
    public void sendTestMail(String email) {
        //Sends test mail
        JavaMailSenderImpl mailSend = new JavaMailSenderImpl();
        mailSend.setHost("smtp.gmail.com");
        mailSend.setPort(465  );

        mailSend.setUsername("flightpubgroup3@gmail.com");
        mailSend.setPassword("FlightPubTestPassword");

        Properties propert = mailSend.getJavaMailProperties();
        propert.put("mail.transport.protocol", "smtps");
        propert.put("mail.smtp.auth", "true");
        propert.put("mail.smtp.starttls.enable", "true");
        propert.put("mail.debug", "true");

        SimpleMailMessage thisMessage = new SimpleMailMessage();
        thisMessage.setTo(email);
        thisMessage.setText("test");
        thisMessage.setSubject("test");
        mailSend.send(thisMessage);
    }
}
