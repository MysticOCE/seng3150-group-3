//Controller object for search page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.Utility;
import seng3150.group3.beans.Flights;
import seng3150.group3.beans.User;
import seng3150.group3.beans.SearchInfo;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.service.FlightSearch;
import seng3150.group3.service.FlightSearchI;
import seng3150.group3.service.UserDAOI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;

@Controller
@RequestMapping("/search")
public class SearchController {
    @Autowired
    ServletContext sc;

    @Autowired
    UserDAOI userService;

    @Autowired
    FlightDAOI flightService;

    /*@Autowired
    FlightSearchI flightSearch;*/

    @GetMapping
    @PostMapping
    //Pre: User has gone to the search page
    //Post: User is redirected to the search page with the search info
    public ModelAndView search() {
        ModelAndView view = new ModelAndView("secured/search");
        User user = userService.getUser(2);
        view.addObject("user", user);
        List<User> users = userService.getAllUsers();
        User user2 = users.get(2);
        view.addObject("user2",user2);
        view.addObject("users",users);
        view.addObject("searchInfo", new SearchInfo());
        //Utility.setupConfig();
        //String checkProperty = Utility.getProperty("testRoot");
        //view.addObject("checkProperty",checkProperty);
        DateFormat dtf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
        /*try {
            //flightSearch.search("ATL", "MIA", LocalDateTime.parse("2017-09-23 11:50:00", dtf), 0, "");
            //List<Flights> f = flightService.getFlightsForSearch("ATL", "MIA", LocalDateTime.parse("2017-09-23 11:50:00", dtf));
            List<Flights> f = flightService.getAllFlights();
            for (Flights fi : f) {
                System.out.println(fi.getAirline().getAirlineCode() + fi.getArrivalTime() + fi.getDuration());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /*try {
            sc.log("\n#####################################Printing flights: ##########################################");  //DEBUG

            List<Flights> f = flightService.getFlightsForSearch("ATL", "MIA", dtf.parse("2017-09-23 11:50:00"));
            for (Flights fi : f) {

                sc.log(fi.getAirline().getAirlineCode() + fi.getArrivalTime() + fi.getDuration());  //DEBUG
            }
            sc.log("Flight printing done");
        }catch(Exception e) {
            sc.log(e.getMessage());
        }*/
        return view;
    }
}
