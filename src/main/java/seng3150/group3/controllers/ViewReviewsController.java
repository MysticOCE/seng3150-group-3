//Controller object for view reviews page.
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.beans.Flights;
import seng3150.group3.beans.Review;
import seng3150.group3.beans.Tag;
import seng3150.group3.beans.User;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.service.ReviewDAOI;
import seng3150.group3.service.UserDAOI;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/view-reviews")
public class ViewReviewsController {
    @Autowired
    ReviewDAOI reviewService;

    @Autowired
    UserDAOI userService;

    @Autowired
    FlightDAOI flightService;

    @GetMapping
    //Pre: User has navigated to view reviews page
    //Post: User is sent to page where they can view all reviews for the flight
    public ModelAndView viewReviews(@RequestParam(value="flight", required = true) int flight) {
        ModelAndView view = new ModelAndView("secured/view-reviews");
        List<Review> reviews = reviewService.getFlightReviews(flight);
        List<List<Tag>> tagLists = new ArrayList<>();
        List<User> users = new ArrayList<>();
        //we'll use varStatus in our c:forEach loop to get the tags out
        for(Review review: reviews) {
            List<Tag> tags = reviewService.getReviewTags(review.getReviewID());
            tagLists.add(tags);
            User user = userService.getUser(review.getUserID());
            users.add(user);
        }
        Flights reviewedFlight = flightService.getFlight(flight);
        view.addObject("flight",reviewedFlight);
        view.addObject("users", users);
        view.addObject("taglists",tagLists);
        view.addObject("reviews", reviews);
        return view;
    }
}
