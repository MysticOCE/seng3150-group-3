//Controller object for the flights detail page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.Utility;
import seng3150.group3.beans.FlightLeg;
import seng3150.group3.beans.Flights;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/flight-details")
public class FlightDetailsController {
    @Autowired
    Utility util;

    @GetMapping
    public ModelAndView flightDetails(@RequestParam(name="id", required = true) String id) {
        ModelAndView view = new ModelAndView("secured/flight-details");

        LinkedList<Flights> multiFlight = util.getMultiFlight(id);

        List<FlightLeg> flightLegs = Utility.getLegs(multiFlight);

        view.addObject("legs", flightLegs);
        view.addObject("flights", multiFlight);
        view.addObject("id", id);
        view.addObject("totalDuration", Utility.totalDuration(multiFlight));
        return view;
    }
}
