//Controller object for login page
package seng3150.group3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class LoginController {
    @GetMapping
    public ModelAndView login() {
        ModelAndView view = new ModelAndView("login");
        return view;
    }

    @PostMapping
    public ModelAndView loginPost() {
        ModelAndView view = new ModelAndView("login");
        return view;
    }
}
