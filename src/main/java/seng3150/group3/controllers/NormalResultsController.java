//Example normal results controller page.
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.Utility;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.beans.Flights;
import seng3150.group3.beans.SearchInfo;
import seng3150.group3.service.FlightSearchI;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/normal-results")
public class NormalResultsController {
    @Autowired
    FlightSearchI searchService;

    @GetMapping(params = { "departureAirport", "arrivalAirport", "departureDate", "returnDate", "passengers", "flightClass" })
    public ModelAndView normalResults(@ModelAttribute SearchInfo searchInfo, BindingResult result, ModelMap model) {
        ModelAndView view = new ModelAndView("secured/normal-results");

        LocalDate departureDate = LocalDate.parse(searchInfo.getDepartureDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<List<Flights>> flightList = searchService.search(
            searchInfo.getDepartureAirport(),
            searchInfo.getArrivalAirport(),
            Date.from(departureDate.atStartOfDay(ZoneId.of("UTC")).toInstant()),
            searchInfo.getPassengers(),
            searchInfo.getFlightClass());

        // Turn the flightList into a list of FlightInfo beans
        List<FlightInfo> flights = Utility.getFlightInfo(flightList);

        view.addObject("flights", flights);
        return view;
    }
}
