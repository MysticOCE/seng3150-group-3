package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import seng3150.group3.beans.Flights;
import seng3150.group3.beans.Review;
import seng3150.group3.beans.TagName;
import seng3150.group3.beans.User;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.service.ReviewDAOI;
import seng3150.group3.service.UserDAOI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/create-review")
//Controller object for the reviews page.
public class CreateReviewController {


    @Autowired
    FlightDAOI flightService;

    @Autowired
    ReviewDAOI reviewService;

    @Autowired
    UserDAOI userService;

    @GetMapping
    //Pre: User has opted to review a thing and a flightID was provided by the JSP
    //Post: Returns a page where you can create a review
    public ModelAndView createReview(@RequestParam(value="flight", required = true) int flight) {
        Flights realFlight = flightService.getFlight(flight);
        // DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); //STUB START
        // LocalDateTime ldt = LocalDateTime.parse("2017-09-23 09:50:00",format);
        // ZonedDateTime zdt = ldt.atZone(ZoneId.of("UTC"));
        // Date tempDate = Date.from(zdt.toInstant());
        // Flights realFlight = flightService.getFlight("AA", "AA1735", tempDate); //STUB END
        List<TagName> tags = reviewService.getAllTagNames();
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ModelAndView view = new ModelAndView("secured/create-review");
        view.addObject("tags", tags);
        view.addObject("flight", realFlight);
        Review review = new Review();
        view.addObject("review", review);
        return view;
    }

    @PostMapping
    //Pre: User has submitted a review
    //Post: Review is added to database and user is redirected to tag adding page
    public String submit(@ModelAttribute("review") Review review, BindingResult result, ModelMap model, RedirectAttributes redir) {
        //User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.verifyUser(review.getEmail());
        if (user == null){
            return "redirect:search";
        }
        review.setUserID(user.getUserID());
        if (! userService.checkUser(review.getEmail(), review.getPassword())) {
            //need to make return error page
            return "redirect:search";
        }
        reviewService.addReview(review);
        String reviewID = String.valueOf(review.getReviewID());
        redir.addAttribute("review",reviewID);
        //ModelAndView view = new ModelAndView(url);
        return "redirect:add-tags";
        //return url;
    }
}
