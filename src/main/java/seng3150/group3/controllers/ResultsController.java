//Controller object for the results page
package seng3150.group3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.beans.SearchInfo;

@Controller
@RequestMapping("/results")
public class ResultsController {
    @GetMapping
    public ModelAndView searchResults() {
        ModelAndView view = new ModelAndView("secured/results");
        return view;
    }
}
