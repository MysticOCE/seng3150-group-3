//Controller object for admin view reviews page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.beans.*;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.service.ReviewDAOI;
import seng3150.group3.service.UserDAO;
import seng3150.group3.service.UserDAOI;

import javax.jws.WebParam;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/view-reviews-admin")
public class ViewReviewsAdminController {
    @Autowired
    ReviewDAOI reviewService;

    @Autowired
    UserDAOI userService;

    @Autowired
    FlightDAOI flightService;

    @GetMapping
    //Pre: Admin has navigated to view reviews page
    //Post: Admin is sent to page where they can view all reviews for the flight and add/delete tag categories
    public ModelAndView viewReviewsAdmin(@RequestParam(value="flight", required = true)int flight) {
        List<Review> reviews = reviewService.getFlightReviews(flight);
        List<TagName> tagNames = reviewService.getAllTagNames();
        List<User> users = new ArrayList<User>();
        List<List<Tag>> tags = new ArrayList<List<Tag>>();
        //Getting the tags and user for each review
        for (Review review: reviews) {
            List<Tag> tag = reviewService.getReviewTags(review.getReviewID());
            tags.add(tag);
            User user = userService.getUser(review.getUserID());
            users.add(user);
        }
        Flights reviewedFlight = flightService.getFlight(flight);
        ModelAndView view = new ModelAndView("secured/view-reviews-admin");
        TagName newTag = new TagName();
        TagName deleteTag = new TagName();
        view.addObject("flight", reviewedFlight);
        view.addObject("deleteTag", deleteTag);
        view.addObject("newTag", newTag);
        view.addObject("users", users);
        view.addObject("tags", tags);
        view.addObject("tagNames", tagNames);
        view.addObject("reviews", reviews);
        return view;
    }
    //Pre: Admin has clicked "delete" on a review
    //Post: Review is deleted and admin is sent to search page
    @RequestMapping(value = "/deleteReview",params = "targetReview", method = RequestMethod.GET)
    public String deleteReview(@RequestParam(value="targetReview") int reviewID) {
        reviewService.deleteReview(reviewID);
        return "redirect:/";
    }
    //Pre: Admin has chosen to add a tag category
    //Post: Tag category is added
    @RequestMapping(params = "addTag", method = RequestMethod.POST)
    public String addTag(@ModelAttribute TagName newTag, BindingResult result, ModelMap model) {
        reviewService.addTagName(newTag);
        return "redirect:/";
    }
}
