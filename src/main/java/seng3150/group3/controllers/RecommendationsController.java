//Controller object for View Recommendations page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import seng3150.group3.beans.Flights;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.Recommend;
import java.util.ArrayList;
import java.util.List;
import seng3150.group3.Utility;
import seng3150.group3.beans.SearchInfo;
import seng3150.group3.service.FlightSearchI;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/recommendations")
public class RecommendationsController {
    @Autowired
    FlightSearchI searchService;

    @GetMapping(params = { "departureAirport", "arrivalAirport", "departureDate", "returnDate", "passengers", "flightClass" })
    public ModelAndView recommendations(@ModelAttribute SearchInfo searchInfo, BindingResult result, ModelMap model) {
        ModelAndView view = new ModelAndView("secured/recommendations");

        LocalDate departureDate = LocalDate.parse(searchInfo.getDepartureDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<List<Flights>> SearchList = searchService.search(
            searchInfo.getDepartureAirport(),
            searchInfo.getArrivalAirport(),
            Date.from(departureDate.atStartOfDay(ZoneId.of("UTC")).toInstant()),
            searchInfo.getPassengers(),
            searchInfo.getFlightClass());

        Recommend rec = new Recommend();
        //List<List<Flights>> SearchList = new ArrayList<>();
        List<Recommend.ScoredFlight> recList = rec.getRecommend(SearchList); // get proper argument for Searchlist input
        List<List<Flights>> list = new ArrayList<>(25);
        for (int i = 0; i < 25 && i < recList.size(); i++) {
            List<Flights> flight =  recList.get(i).getFlight();
            list.add(flight);
        }
        // Turn the flightList into a list of FlightInfo beans
        List<FlightInfo> flightsInfo = Utility.getFlightInfo(list);
        view.addObject("flights", flightsInfo);
        return view;
    }
}
