//Controller object for purchase summary page
package seng3150.group3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.mail.javamail.*;
import org.springframework.mail.SimpleMailMessage;

import seng3150.group3.beans.User;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.beans.Flights;
import seng3150.group3.service.UserDAOI;
import seng3150.group3.service.BookingDAOI;
import seng3150.group3.service.FlightDAOI;
import seng3150.group3.Utility;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Controller
@RequestMapping("/purchase-summary")
public class PurchaseSummaryController {
    @Autowired
    UserDAOI userService;

    @Autowired
    BookingDAOI bookingService;
	
    @Autowired
    FlightDAOI flightService;

    @GetMapping
    public ModelAndView purchaseSummary(@RequestParam(name="email", required = false) String email, @RequestParam(name="sendemail", required = false) String sendemail, @RequestParam(name="flights", required = false) String flights) {
        ModelAndView view = new ModelAndView("secured/purchase-summary");
        //TODO: Verify if email is valid.
        if(email != null) {
			checkLogin(email);      //Checks user based on email.
			if(sendemail != null)
			{
				sendBookedMail(email);	//Sends an email
			}
        }
		if(flights != null)
		{
			//Returns flights for view.
			String[] getFlights = flights.split(",");
			//Creates list of flights
			List<Flights> flightsList = new ArrayList<>();
			double totalPrice = 0;
			for(int i = 0; i < getFlights.length; i++)
			{
				flightsList.add(flightService.getFlight(Integer.parseInt(getFlights[i])));
				totalPrice += flightService.getFlight(Integer.parseInt(getFlights[i])).getPrice();
			}
			view.addObject("flights", Utility.singleFlightInfos(flightsList));
			//view.addObject("flightTotalPrice", totalPrice);	Add this back in when price is not randomised.
		}
		
        return view;
    }
    public void checkLogin(String email) {
        //Checks user table based on email.
        //If there is a userID associated with that email, sets the bookings to that userID
        //If there is not, creates a new userID with that email, and sets the bookings to that userID.
        User getUser = userService.verifyUser(email);
        if(getUser == null) {
            //Gets list of all users
            List<User> allUsers = userService.getAllUsers();    //Gets the id of the latest user to pass to new user ID.
            //Creates a new user.
            User newUser = new User(allUsers.get(allUsers.size()-1).getUserID()+1, "3Test3", "3Test3", email, "3Test3", "3Test3");
            userService.addUser(newUser);
            getUser = newUser;
            //TODO: set user role?
        }
        //assigns bookings to userID of user
        bookingService.setUserPurchaseHistory(getUser.getUserID());//getUser.getUserID());
    }
    public void sendBookedMail(String email) {
        //Sends mail to client
        JavaMailSenderImpl mailSend = new JavaMailSenderImpl();
        mailSend.setHost("smtp.gmail.com");
        mailSend.setPort(465);

        mailSend.setUsername("flightpubgroup3@gmail.com");
        mailSend.setPassword("FlightPubTestPassword");

        Properties propert = mailSend.getJavaMailProperties();
        propert.put("mail.transport.protocol", "smtps");
        propert.put("mail.smtp.auth", "true");
        propert.put("mail.smtp.starttls.enable", "true");
        propert.put("mail.debug", "true");

        SimpleMailMessage thisMessage = new SimpleMailMessage();
        thisMessage.setTo(email);
        thisMessage.setText("Thank you for your booking through FlightPub!\n"); 
        thisMessage.setSubject("Congratulations on your booking!");
        mailSend.send(thisMessage);
        //checks login
    }
}
