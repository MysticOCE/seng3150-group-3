//Bean holding availability of seats in a flight.
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class Availability implements Serializable {

    /*private String countryCode3;*/
    /*@Id
    @Column(name = "AirlineCode")//FK -> Airlines.AirlineCode
    private String airlineCode;
    @Id
    @Column(name = "FlightNumber", nullable = false)
    private String flightNumber;

    @Id
    @Column(name = "DepartureTime")
    private LocalDateTime departureTime;
    @Id
    @Column(name = "ClassCode", nullable = false)//FK -> TicketClass.ClassCode
    private String classCode;
    @Id
    @Column(name = "TicketCode", nullable = false)//FK -> TicketType.TicketCode
    private char ticketCode;*/

    @EmbeddedId//fix FKs
    private AvailabilityPK key;

    private int numberAvailableSeats;

    public Availability() {}

    public Availability(String airlineCode, String flightNumber, int numberAvailableSeats, Date departureTime, String classCode, char ticketCode) {
        //this.countryCode3 = countryCode3;
        /*this.airlineCode = airlineCode;
        this.flightNumber = flightNumber;
        this.numberAvailableSeats = numberAvailableSeats;
        this.departureTime = departureTime;
        this.classCode = classCode;
        this.ticketCode = ticketCode;*/
        key.setAirlineCode(airlineCode);
        key.setFlightNumber(flightNumber);
        key.setDepartureTime(departureTime);
        key.setClassCode(classCode);
        key.setTicketCode(ticketCode);
        this.numberAvailableSeats = numberAvailableSeats;
    }

    public AvailabilityPK getKey() {
        return key;
    }

    public void setKey(AvailabilityPK key) {
        this.key = key;
    }

    public int getNumberAvailableSeats() {
        return numberAvailableSeats;
    }

    public void setNumberAvailableSeats(int numberAvailableSeats) {
        this.numberAvailableSeats = numberAvailableSeats;
    }
}
