//Bean holding type of plane and seat details.
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

@Entity(name = "PlaneType")
@Table(name = "PlaneType")
public class PlaneType implements Serializable {
    @Id
    @Column(name = "PlaneCode", nullable = false)
    private String planeCode;
    @Column(name = "Details")
    private String details;
    @Column(name = "NumFirstClass")
    private int numFirstClass;
    @Column(name = "NumBusiness")
    private int numBusiness;
    @Column(name = "NumPremiumEconomy")
    private int numPremiumEconomy;
    @Column(name = "Economy")
    private int numEconomy;

    @OneToMany(mappedBy = "planeType")
    private Set<Flights> flights;

    public PlaneType() {}

    public PlaneType(String planeCode, String details, int numFirstClass, int numBusiness, int numPremiumEconomy, int numEconomy) {
        this.planeCode = planeCode;
        this.details = details;
        this.numFirstClass = numFirstClass;
        this.numBusiness = numBusiness;
        this.numPremiumEconomy = numPremiumEconomy;
        this.numEconomy = numEconomy;
    }

    public String getPlaneCode() {
        return planeCode;
    }

    public void setPlaneCode(String planeCode) {
        this.planeCode = planeCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getNumFirstClass() {
        return numFirstClass;
    }

    public void setNumFirstClass(int numFirstClass) {
        this.numFirstClass = numFirstClass;
    }

    public int getNumBusiness() {
        return numBusiness;
    }

    public void setNumBusiness(int numBusiness) {
        this.numBusiness = numBusiness;
    }

    public int getNumPremiumEconomy() {
        return numPremiumEconomy;
    }

    public void setNumPremiumEconomy(int numPremiumEconomy) {
        this.numPremiumEconomy = numPremiumEconomy;
    }

    public int getNumEconomy() {
        return numEconomy;
    }

    public void setNumEconomy(int numEconomy) {
        this.numEconomy = numEconomy;
    }

    public Set<Flights> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flights> flights) {
        this.flights = flights;
    }
}
