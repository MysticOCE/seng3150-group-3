package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class FlightPathPK implements Serializable {

    @Column(name = "DepartureCode")
    private String departureCode;

    @Column(name = "DestinationCode")
    private String destinationCode;

    public FlightPathPK() {}

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }
}
