//Bean holding information on roles
package seng3150.group3.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
public class Role implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="roleID")
    private int roleID;

    @Column(name="roleName")
    private String roleName;

    @ManyToMany(mappedBy = "roles")
    private Collection<User> users;

    public Role() {}

    public Role(int roleID, String roleName) {
        this.roleID = roleID;
        this.roleName = roleName;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
