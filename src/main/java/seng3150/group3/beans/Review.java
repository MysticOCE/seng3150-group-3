//Bean holding information on reviews.
package seng3150.group3.beans;

import javax.persistence.*;

@Entity
@Table(name="Review")
public class Review {
    @Id
    @GeneratedValue
    @Column(name="ReviewID")
    private int reviewID;

    //not getting mapped to other element until flight table done
    @Column(name="RFlight")
    private int flightID;

    @Column(name="RUser")
    private int userID;

    @Column(name="RComment")
    private String comment;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Transient
    private String email;

    @Transient
    private String password;

    public Review() {}

    public Review(int reviewID, int flightID, int userID, String comment) {
        this.reviewID = reviewID;
        this.flightID = flightID;
        this.userID = userID;
        this.comment = comment;
    }

    public Review(int flightID, int userID, String comment, String email, String password) {
        this.flightID = flightID;
        this.userID = userID;
        this.comment = comment;
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Review(int flightID, int userID, String comment, String email) {
        this.flightID = flightID;
        this.userID = userID;
        this.comment = comment;
        this.email = email;
    }

    public int getReviewID() {
        return reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
