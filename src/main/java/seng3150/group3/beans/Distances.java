//Bean holding distance between two places
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Distances implements Serializable {
    @EmbeddedId
    private DistancesPK key;

    @Column(name = "DistancesInKms")
    private int distanceKm;

    @MapsId("destinationCode1")
    @ManyToOne
    @JoinColumn(name = "DestinationCode1", nullable = false, referencedColumnName = "DestinationCode")
    private Destinations destinationCode1;

    @MapsId("destinationCode2")
    @ManyToOne
    @JoinColumn(name = "DestinationCode2", nullable = false, referencedColumnName = "DestinationCode")
    private Destinations destinationCode2;

    public Distances() {}

    /*public Distances(String destinationCode1, String destinationCode2, int distanceKm) {
        this.destinationCode1 = destinationCode1;
        this.destinationCode2 = destinationCode2;
        this.distanceKm = distanceKm;
    }

    public String getDestinationCode1() {
        return destinationCode1;
    }

    public void setDestinationCode1(String destinationCode1) {
        this.destinationCode1 = destinationCode1;
    }

    public String getDestinationCode2() {
        return destinationCode2;
    }

    public void setDestinationCode2(String destinationCode2) {
        this.destinationCode2 = destinationCode2;
    }*/

    public int getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(int distanceKm) {
        this.distanceKm = distanceKm;
    }

    public DistancesPK getKey() {
        return key;
    }

    public void setKey(DistancesPK key) {
        this.key = key;
    }

    public Destinations getDestinationCode1() {
        return destinationCode1;
    }

    public void setDestinationCode1(Destinations destinationCode1) {
        this.destinationCode1 = destinationCode1;
    }

    public Destinations getDestinationCode2() {
        return destinationCode2;
    }

    public void setDestinationCode2(Destinations destinationCode2) {
        this.destinationCode2 = destinationCode2;
    }
}
