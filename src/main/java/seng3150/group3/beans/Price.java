//Bean holding price data for flights
package seng3150.group3.beans;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Price implements Serializable {
    private String airlineCode;
    private String flightNumber;
    private String classCode;
    private char ticketCode;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private double price;

    public Price() {}

    public Price(String airlineCode, String flightNumber, String classCode, char ticketCode, LocalDateTime startDate, LocalDateTime endDate, double price) {
        this.airlineCode = airlineCode;
        this.flightNumber = flightNumber;
        this.classCode = classCode;
        this.ticketCode = ticketCode;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public char getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(char ticketCode) {
        this.ticketCode = ticketCode;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
