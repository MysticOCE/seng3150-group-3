//Bean holding information for user.
package seng3150.group3.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
public class User implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="userID")
    private int userID;

    @Column(name="firstName")
    private String firstName;

    @Column(name="lastName")
    private String lastName;

    @Column(name="email")
    private String email;

    @Column(name="phone")
    private String phone;

    @Column(name="password")
    private String password;

    @ManyToMany
    @JoinTable(
        name="userRoles",
            joinColumns= @JoinColumn(name="userID", referencedColumnName = "userID"),
        inverseJoinColumns=@JoinColumn(name="roleID", referencedColumnName = "roleID")
        )
    private Collection<Role> roles;

    @ManyToMany(mappedBy = "users")
    private Collection<Booking> bookings;

    public User() {}

    public User(int userID, String firstName, String lastName, String email, String phone, String password) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() { return roles; }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
