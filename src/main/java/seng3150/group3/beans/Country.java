//Bean holding country information.
package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Country implements Serializable {
    @Column(name = "countryCode2")
    private String countryCode2;

    @Id
    @Column(name = "countryCode3", nullable = false)
    private String countryCode3;

    @Column(name = "alternateName1")
    private String alternateName1;

    @Column(name = "alternateName2")
    private String alternateName2;

    @Column(name = "motherCountryCode3")
    private String motherCountryCode3;

    @Column(name = "motherCountryComment")
    private String motherCountryComment;

    @OneToMany(mappedBy = "country")
    private Set<Airlines> airlines;

    @OneToMany(mappedBy = "countryCode3")
    private Set<Destinations> destinations;

    public Country() {}

    /*public Country(String countryCode3, String countryCode2, String alternateName1, String alternateName2) {
        this.countryCode3 = countryCode3;
        this.countryCode2 = countryCode2;
        this.alternateName1 = alternateName1;
        this.alternateName2 = alternateName2;
    }*/

    public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }

    public String getCountryCode2() {
        return countryCode2;
    }

    public void setCountryCode2(String countryCode2) {
        this.countryCode2 = countryCode2;
    }

    public String getAlternateName1() {
        return alternateName1;
    }

    public void setAlternateName1(String alternateName1) {
        this.alternateName1 = alternateName1;
    }

    public String getAlternateName2() {
        return alternateName2;
    }

    public void setAlternateName2(String alternateName2) {
        this.alternateName2 = alternateName2;
    }

    public String getMotherCountryCode3() {
        return motherCountryCode3;
    }

    public void setMotherCountryCode3(String motherCountryCode3) {
        this.motherCountryCode3 = motherCountryCode3;
    }

    public String getMotherCountryComment() {
        return motherCountryComment;
    }

    public void setMotherCountryComment(String motherCountryComment) {
        this.motherCountryComment = motherCountryComment;
    }

    public Set<Airlines> getAirlines() {
        return airlines;
    }

    public void setAirlines(Set<Airlines> airlines) {
        this.airlines = airlines;
    }

    public Set<Destinations> getDestinations() {
        return destinations;
    }

    public void setDestinations(Set<Destinations> destinations) {
        this.destinations = destinations;
    }
}
