//Bean holding data on ticket type.
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class TicketType implements Serializable {
    @Id
    @Column(name = "TicketCode")
    private char ticketCode;

    @Column(name = "Name")
    private String name;

    @Column(name = "Transferrable")
    private boolean transferrable;

    @Column(name = "Refundable")
    private boolean refundable;

    @Column(name = "Exchangeable")
    private boolean exchangeable;

    @Column(name = "FrequentFlyerPoints")
    private boolean frequentFlyerPoints;

    /*@OneToMany(mappedBy = "TicketCode")
    private Set<Availability> availability;*/

    public TicketType() {}

    public TicketType(char ticketCode, String name, boolean transferrable, boolean refundable, boolean exchangeable, boolean frequentFlyerPoints) {
        this.ticketCode = ticketCode;
        this.name = name;
        this.transferrable = transferrable;
        this.refundable = refundable;
        this.exchangeable = exchangeable;
        this.frequentFlyerPoints = frequentFlyerPoints;
    }

    public char getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(char ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTransferrable() {
        return transferrable;
    }

    public void setTransferrable(boolean transferrable) {
        this.transferrable = transferrable;
    }

    public boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(boolean refundable) {
        this.refundable = refundable;
    }

    public boolean isExchangeable() {
        return exchangeable;
    }

    public void setExchangeable(boolean exchangeable) {
        this.exchangeable = exchangeable;
    }

    public boolean isFrequentFlyerPoints() {
        return frequentFlyerPoints;
    }

    public void setFrequentFlyerPoints(boolean frequentFlyerPoints) {
        this.frequentFlyerPoints = frequentFlyerPoints;
    }

    /*public Set<Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(Set<Availability> availability) {
        this.availability = availability;
    }*/
}
