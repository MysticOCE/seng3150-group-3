package seng3150.group3.beans;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Immutable
@Table(name = "FlightPaths")
public class FlightPath implements Serializable {
    @EmbeddedId
    private FlightPathPK key;

    @Column(name = "Duration")
    private int duration;

    public FlightPath() {}

    public String getDepartureCode() {
        return key.getDepartureCode();
    }

    public void setDepartureCode(String departureCode) {
        key.setDepartureCode(departureCode);
    }

    public String getDestinationCode() {
        return key.getDestinationCode();
    }

    public void setDestinationCode(String destinationCode) {
        key.setDestinationCode(destinationCode);
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
