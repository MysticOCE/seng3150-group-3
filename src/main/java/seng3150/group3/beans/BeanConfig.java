//Configuration file for beans.
package seng3150.group3.beans;

import org.springframework.context.annotation.*;

@Configuration
public class BeanConfig {
    @Bean
    public Airlines airlines() {
        return new Airlines();
    }

    @Bean
    public Availability availability() {
        return new Availability();
    }

    @Bean
    public Country country() {
        return new Country();
    }

    @Bean
    public Booking booking() {return new Booking();}

    @Bean
    public BookingOrder bookingOrder() {return new BookingOrder();}

    @Bean
    public Destinations destination() {
        return new Destinations();
    }

    @Bean
    public Distances distance() {
        return new Distances();
    }

    @Bean
    public Flights flights() {
        return new Flights();
    }

    @Bean
    public FlightPath flightPath() { return new FlightPath(); }
    /*@Bean
    public MotherCountry motherCountry() {
        return new MotherCountry();
    }*/

    @Bean
    public PlaneType planeType() {
        return new PlaneType();
    }

    @Bean
    public Price price() {
        return new Price();
    }

    @Bean
    public PurchaseHistory purchaseHistory() {return new PurchaseHistory();}

    @Bean
    public Review review() {return new Review();}

    @Bean
    public Role role() {
        return new Role();
    }

    @Bean
    public Tag tag() { return new Tag();}

    @Bean
    public TagName tagName() {return new TagName();}

    @Bean
    public TicketClass ticketClass() {
        return new TicketClass();
    }

    @Bean
    public TicketType ticketType() {
        return new TicketType();
    }

    @Bean
    public User user() {
        return new User();
    }
}
