//Bean holding information on a direct flight from arrival to destination
package seng3150.group3.beans;

import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "Flights")
public class Flights implements Serializable {

    @Column(name = "id")
    private int id;

    /*@Column(name = "AirlineCode", nullable = false)//FK -> Airlines.AirlineCode
    private String airlineCode;*/

    /*@Id
    @Column(name = "FlightNumber", nullable = false)
    private String flightNumber;*/

   /* @Column(name = "DepartureCode")//FK -> Destinations.DestinationCode
    private String departureCode;

    @Column(name = "DestinationCode")//FK -> Destinations.DestinationCode
    private String destinationCode;*/

    /*@Id
    @Column(name = "DepartureTime", nullable = false)
    private LocalDateTime departureTime;*/

    @Column(name = "ArrivalTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalTime;

    /*@Column(name = "PlaneCode")//FK -> PlaneType.PlaneCode
    private String planeCode;*/

    @Column(name = "Duration")
    private int duration;

    /*
     * Contains composite Primary Key:
     * AirlineCode
     * FlightNumber
     * DepartureTime
     */
    @EmbeddedId
    private FlightsPK key;

    @MapsId("airlineCode")
    @JoinColumn(name = "AirlineCode", referencedColumnName = "AirlineCode")
    @ManyToOne
    private Airlines airline;

    @JoinColumn(name = "DestinationCode", nullable = false)
    @ManyToOne
    private Destinations destinationCode;

    @JoinColumn(name = "DepartureCode", nullable = false)
    @ManyToOne
    private Destinations departureCode;

    @JoinColumn(name = "PlaneCode", nullable = false)
    @ManyToOne
    private PlaneType planeType;

    @Transient
    private double price;//just set arbitrarily

    public Flights() {}

    public Flights(String airlineCode, String flightNumber, Date departureTime, int id, String departureCode, int duration, Date arrivalTime, String destinationCode, String planeCode) {
        key = new FlightsPK();
        key.setAirlineCode(airlineCode);
        key.setDepartureTime(departureTime);
        key.setFlightNumber(flightNumber);
        this.id = id;

        this.duration = duration;
        this.arrivalTime = arrivalTime;
    }

    // public void setAirlineCode(String airlineCode) {
    //     this.airlineCode = airlineCode;
    // }

    // public void setFlightNumber(String flightNumber) {
    //     this.flightNumber = flightNumber;
    // }

    // public void setDepartureTime(LocalDateTime departureTime) {
    //     this.departureTime = departureTime;
    // }

    // public void setDepartureCode(String departureCode) {
    //     this.departureCode = departureCode;
    // }

    // public void setDestinationCode(String destinationCode) {
    //     this.destinationCode = destinationCode;
    // }

    // public void setPlaneCode(String planeCode) {
    //     this.planeCode = planeCode;
    // }

    public String getAirlineCode() {
        return key.getAirlineCode();
    }

    public String getFlightNumber() {
        return key.getFlightNumber();
    }

    public Date getDepartureTime() {
        return key.getDepartureTime();
    }

    public String getDepartureCodeString() {
        // I *think* getDestinationCode() is correct. TODO delete comment
        return departureCode.getDestinationCode();
    }

    public String getDestinationCodeString() {
        return destinationCode.getDestinationCode();
    }

    public String getPlaneCode() {
        return planeType.getPlaneCode();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public PlaneType getPlaneType() {
        return planeType;
    }

    public void setPlaneType(PlaneType planeType) {
        this.planeType = planeType;
    }

    public FlightsPK getKey() {
        return key;
    }

    public void setKey(FlightsPK key) {
        this.key = key;
    }

    public Airlines getAirline() {
        return airline;
    }

    public void setAirline(Airlines airline) {
        this.airline = airline;
    }

    public Destinations getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(Destinations departureCode) {
        this.departureCode = departureCode;
    }

    public Destinations getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(Destinations destinationCode) {
        this.destinationCode = destinationCode;
    }

    public double getPrice() {
        if(duration != 0)
            if(price == 0)
                price = (Math.random() * 4 + 2 ) * duration;//from 2->4 times duration, if not already set

        return price;
    }

    /**
     * Just set this value based on duration * some random value
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }
}
