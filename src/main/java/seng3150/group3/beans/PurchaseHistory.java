//Bean holding information on user purchase history
package seng3150.group3.beans;

import java.io.Serializable;
import java.util.Collection;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.*;

@Entity
public class PurchaseHistory implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="purchaseHistoryID")
    private int purchaseHistoryID;

    @Column(name="userID")
    private int userID;

    @Column(name="bookingID")
    private int bookingID;

    //@Column(name="inputTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inputTime; //Time this was put into purchaseHistory

    /*
    @ManyToMany(mappedBy = "bookingOrders")
    private Collection<Booking> bookings;// = new Collection<>();;
    */

    public PurchaseHistory() {}

    public PurchaseHistory(int purchaseHistoryID, int userID, int bookingID, Date inputTime) {
        this.purchaseHistoryID = purchaseHistoryID;
        this.userID = userID;
        this.bookingID = bookingID;
        this.inputTime = inputTime;
    }

    public int getPurchaseHistoryID() {
        return purchaseHistoryID;
    }

    public void setPurchaseHistoryID(int purchaseHistoryID) {
        this.purchaseHistoryID = purchaseHistoryID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public Date getInputTime() {
        return inputTime;
    }

    public void setInputTime(Date inputTime) {
        this.inputTime = inputTime;
    }
}
