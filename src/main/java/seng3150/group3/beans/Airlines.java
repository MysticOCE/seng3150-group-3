//Bean holding airline information.
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Airlines implements Serializable {
    @Id
    @Column(name = "AirlineCode")
    private String airlineCode;

    @Column(name = "AirlineName")
    private String airlineName;

    /*@Column(name = "CountryCode3")//FK -> Country.countryCode3
    private String countryCode3;*/

    @JoinColumn(name = "countryCode3", referencedColumnName = "CountryCode3", nullable = false)
    @ManyToOne
    private Country country;

    @OneToMany(mappedBy = "airline")
    private Set<Flights> flights;

    public Airlines() {}

    /*public Airlines(String airlineCode, String airlineName, String countryCode3) {
        this.airlineCode = airlineCode;
        this.airlineName = airlineName;
        this.countryCode3 = countryCode3;
    }*/

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Flights> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flights> flights) {
        this.flights = flights;
    }
/*public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }*/
}
