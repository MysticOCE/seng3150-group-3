//Bean holding information on overall ticket, including stopovers and price information.
package seng3150.group3.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

public class FlightInfo implements Serializable {
    private String airlineName;
    private String flightNumber;
    private String departureTime;
    private String departureDate;
    private String departureCity;
    private String duration;
    private String arrivalTime;
    private String arrivalDate;
    private String destinationCity;
    private String planeCode;
    private String airports;
    private String imgsrc;
    private String stops;
    private String price;
    private String id;

    public FlightInfo() {}

    public FlightInfo(String airlineName,
                      String flightNumber,
                      String departureTime,
                      String departureDate,
                      String departureCity,
                      String duration,
                      String arrivalTime,
                      String arrivalDate,
                      String destinationCity,
                      String planeCode,
                      String airports,
                      String imgsrc,
                      String stops,
                      String price,
                      String id) {
        this.airlineName = airlineName;
        this.flightNumber = flightNumber;
        this.departureTime = departureTime;
        this.departureDate = departureDate;
        this.departureCity = departureCity;
        this.duration = duration;
        this.arrivalTime = arrivalTime;
        this.arrivalDate = arrivalDate;
        this.destinationCity = destinationCity;
        this.planeCode = planeCode;
        this.airports = airports;
        this.imgsrc = imgsrc;
        this.stops = stops;
        this.price = price;
        this.id = id;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getPlaneCode() {
        return planeCode;
    }

    public void setPlaneCode(String planeCode) {
        this.planeCode = planeCode;
    }

    public String getAirports() {
        return airports;
    }

    public void setAirports(String airports) {
        this.airports = airports;
    }

    public String getImgsrc() {
        return imgsrc;
    }

    public void setImgsrc(String imgsrc) {
        this.imgsrc = imgsrc;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
