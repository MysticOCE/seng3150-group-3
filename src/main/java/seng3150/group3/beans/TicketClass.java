//Bean holding data on ticket class
package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Set;

@Entity
public class TicketClass implements Serializable {
    @Id
    @Column(name = "ClassCode")
    private String classCode;

    @Column(name = "Details")
    private String details;

    /*@OneToMany(mappedBy = "ClassCode")
    private Set<Availability> availability;*/

    /*
    Price
    private Set<Price> price;
     */

    public TicketClass() {}

    public TicketClass(String classCode, String details) {
        this.classCode = classCode;
        this.details = details;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    /*public Set<Availability> getAvailability() {
        return availability;
    }

    public void setAvailability(Set<Availability> availability) {
        this.availability = availability;
    }*/
}
