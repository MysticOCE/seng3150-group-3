//Bean holding information on review tags.
package seng3150.group3.beans;

import javax.persistence.*;

@Entity
@Table(name="Tags")
public class Tag {
    @Id
    @GeneratedValue
    @Column(name="TagID")
    private int tagID;

    @Column(name="ReviewID")
    private int reviewID;

    @Column(name="TagName")
    private String tagName;

    @Column(name="TagScore")
    private int tagScore;

    public Tag() {}

    public Tag(int tagID, int reviewID, String tagName, int tagScore) {
        this.tagID = tagID;
        this.reviewID = reviewID;
        this.tagName = tagName;
        this.tagScore = tagScore;
    }

    public int getTagID() {
        return tagID;
    }

    public void setTagID(int tagID) {
        this.tagID = tagID;
    }

    public int getReviewID() {
        return reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public int getTagScore() {
        return tagScore;
    }

    public void setTagScore(int tagScore) {
        this.tagScore = tagScore;
    }
}
