package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DistancesPK implements Serializable {
    @Column(name = "DestinationCode1")
    private String destinationCode1;

    @Column(name = "DestinationCode2")
    private String destinationCode2;

    public DistancesPK() {}

    public String getDestinationCode1() {
        return destinationCode1;
    }

    public void setDestinationCode1(String destinationCode1) {
        this.destinationCode1 = destinationCode1;
    }

    public String getDestinationCode2() {
        return destinationCode2;
    }

    public void setDestinationCode2(String destinationCode2) {
        this.destinationCode2 = destinationCode2;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistancesPK that = (DistancesPK) o;
        return Objects.equals(destinationCode1, that.destinationCode1) &&
                Objects.equals(destinationCode2, that.destinationCode2);
    }

    public int hashCode() {
        return Objects.hash(destinationCode1, destinationCode2);
    }
}
