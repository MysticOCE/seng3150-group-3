//Bean holding information for bookings.
package seng3150.group3.beans;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
public class Booking implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="bookingID")
    private int bookingID;
    /*
    @Column(name="userID")
    private int userID;
    */
    @Column(name="flightID")
    private int flightID;

    @Column(name="classcode")
    private String classcode;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
        name="BookingOrder",
            joinColumns= @JoinColumn(name="bookingID", referencedColumnName = "bookingID"),
        inverseJoinColumns=@JoinColumn(name="userID", referencedColumnName = "userID")
        )
    private Collection<User> users;

    //private Collection<BookingOrder> bookingOrders;// = new Collection<>();

    //private Collection<BookingOrder> bookingOrders;
    /*
    @ManyToMany
    @JoinTable(
        name="UserBookings",
            joinColumns= @JoinColumn(name="classcode", referencedColumnName = "classcode"),
        inverseJoinColumns=@JoinColumn(name="userID", referencedColumnName = "userID")
        )
    private Collection<Flights> flights;
    */
    public Booking() {}

    public Booking(int bookingID, int id, String classcode) {
        this.bookingID = bookingID;
        this.flightID = id;
        this.classcode = classcode;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getClassCode() {
        return classcode;
    }

    public void setClassCode(String classcode) {
        this.classcode = classcode;
    }
}
