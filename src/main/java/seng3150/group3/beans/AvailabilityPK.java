package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

public class AvailabilityPK implements Serializable {
    @Column(name = "AirlineCode")
    private String airlineCode;

    @Column(name = "FlightNumber")
    private String flightNumber;

    @Column(name = "DepartureTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureTime;

    @Column(name = "ClassCode")
    private String classCode;

    @Column(name = "TicketCode")
    private char ticketCode;

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public char getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(char ticketCode) {
        this.ticketCode = ticketCode;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AvailabilityPK that = (AvailabilityPK) o;
        return ticketCode == that.ticketCode &&
                Objects.equals(airlineCode, that.airlineCode) &&
                Objects.equals(flightNumber, that.flightNumber) &&
                Objects.equals(departureTime, that.departureTime) &&
                Objects.equals(classCode, that.classCode);
    }

    public int hashCode() {

        return Objects.hash(airlineCode, flightNumber, departureTime, classCode, ticketCode);
    }
}
