//Bean holding information on review tag categories.
package seng3150.group3.beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tagnames")
public class TagName implements Serializable {
    @Id
    @Column(name = "TagName")
    private String tagName;

    public TagName() {}

    public TagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
