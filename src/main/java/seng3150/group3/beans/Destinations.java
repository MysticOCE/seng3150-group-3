//Bean holding destination information
package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Destinations implements Serializable {
    @Id
    @Column(name = "DestinationCode", nullable = false)
    private String destinationCode;

    @Column(name = "Airport")
    private String airport;

    @Column(name = "CountryCode3")//FK -> Country.countryCode3
    private String countryCode3;

    @OneToMany(mappedBy = "destinationCode1")
    private Set<Distances> destinationCode1;

    @OneToMany(mappedBy = "destinationCode2")
    private Set<Distances> destinationCode2;

    @OneToMany(mappedBy = "departureCode")
    private Set<Flights> departureCodeFlight;

    @OneToMany(mappedBy = "destinationCode")
    private Set<Flights> destinationCodeFlight;

    public Destinations() {}

    /*public Destinations(String destinationCode, String airport, String countryCode3) {
        this.destinationCode = destinationCode;
        this.airport = airport;
        this.countryCode3 = countryCode3;
    }*/

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getCountryCode3() {
        return countryCode3;
    }

    public void setCountryCode3(String countryCode3) {
        this.countryCode3 = countryCode3;
    }

    public Set<Distances> getDestinationCode1() {
        return destinationCode1;
    }

    public void setDestinationCode1(Set<Distances> destinationCode1) {
        this.destinationCode1 = destinationCode1;
    }

    public Set<Distances> getDestinationCode2() {
        return destinationCode2;
    }

    public void setDestinationCode2(Set<Distances> destinationCode2) {
        this.destinationCode2 = destinationCode2;
    }

    public Set<Flights> getDepartureCodeFlight() {
        return departureCodeFlight;
    }

    public void setDepartureCodeFlight(Set<Flights> departureCodeFlight) {
        this.departureCodeFlight = departureCodeFlight;
    }

    public Set<Flights> getDestinationCodeFlight() {
        return destinationCodeFlight;
    }

    public void setDestinationCodeFlight(Set<Flights> destinationCodeFlight) {
        this.destinationCodeFlight = destinationCodeFlight;
    }
}
