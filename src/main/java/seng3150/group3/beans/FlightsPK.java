package seng3150.group3.beans;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Embeddable
public class FlightsPK implements Serializable {

    @Column(name = "AirlineCode")
    private String airlineCode;

    @Column(name = "FlightNumber")
    private String flightNumber;

    @Column(name = "DepartureTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureTime;

    public FlightsPK() {}

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getAirlineCode() { return airlineCode; }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightsPK flightsPK = (FlightsPK) o;
        return Objects.equals(airlineCode, flightsPK.airlineCode) &&
                Objects.equals(flightNumber, flightsPK.flightNumber) &&
                Objects.equals(departureTime, flightsPK.departureTime);
    }

    public int hashCode() {

        return Objects.hash(airlineCode, flightNumber, departureTime);
    }
}
