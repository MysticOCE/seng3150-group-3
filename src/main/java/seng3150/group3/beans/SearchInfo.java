//Bean holding info on search information.
package seng3150.group3.beans;

import java.io.Serializable;

public class SearchInfo implements Serializable {
    private String departureAirport;
    private String arrivalAirport;
    private String departureDate;
    private String returnDate;
    private Integer passengers;
    private String flightClass;

    public SearchInfo() {}

    public SearchInfo(String departureAirport, String arrivalAirport, String departureDate, String returnDate, Integer passengers, String flightClass) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.passengers = passengers;
        this.flightClass = flightClass;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    /* For debugging */
    public String toString() {
        String departureAirportTemp = (departureAirport != null) ? departureAirport.toString() : "";
        String arrivalAirportTemp = (arrivalAirport != null) ? arrivalAirport.toString() : "";
        String departureDateTemp = (departureDate != null) ? departureDate.toString() : "";
        String returnDateTemp = (returnDate != null) ? returnDate.toString() : "";
        String passengersTemp = (passengers != null) ? passengers.toString() : "HI";
        String flightClassTemp = (flightClass != null) ? flightClass.toString() : "";
        return departureAirportTemp + arrivalAirportTemp + departureDateTemp + returnDateTemp + passengersTemp + flightClassTemp;
    }
}
