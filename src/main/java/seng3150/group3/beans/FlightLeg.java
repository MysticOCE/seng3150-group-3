//Bean holding information on overall ticket, including stopovers and price information.
package seng3150.group3.beans;

import java.io.Serializable;

public class FlightLeg implements Serializable {
    private String duration;
    private boolean stopover;
    private String departureDayTime;
    private String departureCity;
    private String departureCode;
    private String arrivalDayTime;
    private String destinationCity;
    private String destinationCode;
    private String airlineName;
    private String flightNumber;
    private String imgsrc;
    private String planeCode;

    public FlightLeg() {}

    public FlightLeg(
            String duration,
            boolean stopover,
            String departureDayTime,
            String departureCity,
            String departureCode,
            String arrivalDayTime,
            String destinationCity,
            String destinationCode,
            String airlineName,
            String flightNumber,
            String imgsrc,
            String planeCode) {
        this.duration = duration;
        this.stopover = stopover;
        this.departureDayTime = departureDayTime;
        this.departureCity = departureCity;
        this.departureCode = departureCode;
        this.arrivalDayTime = arrivalDayTime;
        this.destinationCity = destinationCity;
        this.destinationCode = destinationCode;
        this.airlineName = airlineName;
        this.flightNumber = flightNumber;
        this.imgsrc = imgsrc;
        this.planeCode = planeCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isStopover() {
        return stopover;
    }

    public void setStopover(boolean stopover) {
        this.stopover = stopover;
    }

    public String getDepartureDayTime() {
        return departureDayTime;
    }

    public void setDepartureDayTime(String departureDayTime) {
        this.departureDayTime = departureDayTime;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getArrivalDayTime() {
        return arrivalDayTime;
    }

    public void setArrivalDayTime(String arrivalDayTime) {
        this.arrivalDayTime = arrivalDayTime;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getImgsrc() {
        return imgsrc;
    }

    public void setImgsrc(String imgsrc) {
        this.imgsrc = imgsrc;
    }

    public String getPlaneCode() {
        return planeCode;
    }

    public void setPlaneCode(String planeCode) {
        this.planeCode = planeCode;
    }
}
