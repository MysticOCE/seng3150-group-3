package seng3150.group3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import seng3150.group3.beans.FlightInfo;
import seng3150.group3.beans.FlightLeg;
import seng3150.group3.beans.Flights;
import seng3150.group3.service.FlightDAOI;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
//class containing reusable utility methods
public class Utility {
    @Autowired
    private FlightDAOI flightService;
    private static DateFormat dfDayTime = new SimpleDateFormat("EEE HH:mm");  //"Sat 13:30" TODO check this is 24hr time
    private static DateFormat dfDate = new SimpleDateFormat("EEE, MMM d");
    private static DateFormat dfTime = new SimpleDateFormat("hh:mm a");
    //config loading not working.
    /*
    public static Properties config;

    public static void setupConfig(){
        config = new Properties();
        try{
            config.load(ClassLoader.class.getResourceAsStream());
        }
        catch (Exception e){
            System.out.println("Config file for testing not loaded.");
        }
    }

    public static String getProperty(String key){
        String value = config.getProperty(key);
        return value;
    }
    */

    public static String getTestRoot(){
        return "localhost:8080/FlightPub-0.2/";
    }

    //Pre: Gets a list of flight journeys
    //Post: Returns extra information about that flight journey
    public static List<FlightInfo> getFlightInfo(List<List<Flights>> searchList) {
        List<FlightInfo> flights = new ArrayList<FlightInfo>(10);
        int listSize = searchList.size() < 10 ? searchList.size() : 10;
        for (int i = 0; i < listSize; i++) {
            List<Flights> currentFlight = searchList.get(i);
            int noOfLegs = currentFlight.size();

            String airline = currentFlight.get(0).getAirline().getAirlineName();
            String departureCity = currentFlight.get(0).getDepartureCode().getAirport();
            String arrivalCity = currentFlight.get(currentFlight.size()-1).getDestinationCode().getAirport();
            String airports = "";
            String stops = String.format("%d stop%s", noOfLegs-1, (noOfLegs == 2) ? "" : "s");
            String id = "";

            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            double priceVal = 0;
            for(Flights f : currentFlight) {
                priceVal += f.getPrice();
            }
            String price = formatter.format(priceVal);

            int durationMins = 0;
            for (int j = 0; j < currentFlight.size(); j++) {
                Flights currentLeg = currentFlight.get(j);
                if (j != 0) {
                    airports += "-";
                    id += ",";
                    // Finds stopover time. Assumes that the flights don't overlap
                    durationMins += stopoverMins(currentLeg.getDepartureTime().getTime(), currentFlight.get(j-1).getArrivalTime().getTime());
                }
                airports += currentLeg.getDepartureCodeString();
                id += currentLeg.getId();
                durationMins += currentLeg.getDuration();
                if (j == currentFlight.size()-1) {
                    airports += "-";
                    airports += currentLeg.getDestinationCodeString();
                }
            }
            String duration = durationString(durationMins);

            flights.add(new FlightInfo(
                    airline,  //"American Airlines"
                    currentFlight.get(0).getFlightNumber(),  //"AA1735"
                    dfTime.format(currentFlight.get(0).getDepartureTime()),  //"9:50 AM"
                    dfDate.format(currentFlight.get(0).getDepartureTime()),  //"Sun, Sep 24"
                    departureCity,  //"Atlanta"
                    duration,  //"22h 10min"
                    dfTime.format(currentFlight.get(noOfLegs-1).getArrivalTime()),  //"9:00 AM"
                    dfDate.format(currentFlight.get(noOfLegs-1).getArrivalTime()),  //"Mon, Sep 25"
                    arrivalCity,  //"Rio De Janeiro"
                    currentFlight.get(0).getPlaneCode(),  //"A380"
                    airports,  //"ATL-MIA-GIG"
                    String.format("img/%s.png", currentFlight.get(0).getAirlineCode()),  //"img/AA.png"
                    stops,  //"1 stop"
                    price,  //"$2022.70"
                    id));  //"1,2,3" ie. A list of flight ids delimited by commas
        }
        return flights;
    }

    public LinkedList<Flights> getMultiFlight(String idList) {
        String[] ids = idList.split(",");
        LinkedList<Flights> flights = new LinkedList<Flights>();
        for (String id : ids) {
            flights.add(flightService.getFlight(Integer.parseInt(id)));
        }
        return flights;
    }

    public static List<FlightLeg> getLegs(List<Flights> multiFlight) {
        List<FlightLeg> legs = new ArrayList<FlightLeg>(multiFlight.size() * 2);
        for (int i = 0; i < multiFlight.size(); i++) {
            Flights currentLeg = multiFlight.get(i);
            if (i != 0) {  // Multi-leg -- Needs a stopover leg
                legs.add(getStopoverLeg(multiFlight.get(i-1), currentLeg));
            }
            legs.add(getLeg(currentLeg));
        }
        return legs;
    }

    /* Package-private so that it can be accessed by test classes */
    static FlightLeg getLeg(Flights flight) {
        String departTime = dfDayTime.format(flight.getDepartureTime());
        String arriveTime = dfDayTime.format(flight.getArrivalTime());
        String imgsrc = String.format("img/%s.png", flight.getAirlineCode());
        return new FlightLeg(
                durationString(flight.getDuration()),  // "2h"
                false,  // false
                departTime,  // "Sat 09:50"
                flight.getDepartureCode().getAirport(),  // "Atlanta"
                flight.getDepartureCodeString(),  // "ATL"
                arriveTime,  // "Sat 11:50"
                flight.getDestinationCode().getAirport(),  // "Miami"
                flight.getDestinationCodeString(),  // "MIA"
                flight.getAirline().getAirlineName(),  // "American Airlines"
                flight.getFlightNumber(),  // "AA1735"
                imgsrc,  // "img/AA.png"
                flight.getPlaneCode());  // "A380"
    }

    /* Package-private so that it can be accessed by test classes */
    static FlightLeg getStopoverLeg(Flights flight1, Flights flight2) {
        String duration = durationString(stopoverMins(flight2.getDepartureTime().getTime(), flight1.getArrivalTime().getTime()));
        return new FlightLeg(
                duration,  // "11h 30min"
                true,  // true
                "N/A",  // "Sat 11:50" OLD
                flight1.getDestinationCode().getAirport(),  // "Miami"
                flight1.getDestinationCodeString(),  // "MIA"
                "N/A",  // "Sat 23:20" OLD
                flight1.getDestinationCode().getAirport(),  // "Miami"
                flight1.getDestinationCodeString(),  // "MIA"
                "N/A",  // "N/A"
                "N/A",  // "N/A"
                "N/A",  // "N/A"
                "N/A");  // "N/A"
    }

    /* Package-private so that it can be accessed by test classes */
    static String durationString(int durationMins) {
        return String.format("%dh %dmin", Math.floorDiv(durationMins, 60), durationMins % 60);
    }

    /* Package-private so that it can be accessed by test classes */
    static int stopoverMins(long time1, long time2) {
        long millisecondDiff = Math.abs(time1 - time2);
        return millisecondsToMins(millisecondDiff);
    }

    /* Package-private so that it can be accessed by test classes */
    static int millisecondsToMins(long milli) {
        return (int)Math.floorDiv(milli, 60*1000);
    }

    public static String totalDuration(LinkedList<Flights> multiFlight) {
        int duration = 0;
        for (int i = 0; i < multiFlight.size()*2 - 1; i++) {
            if (i % 2 == 0) {
                duration += multiFlight.get(i/2).getDuration();
            }
            else {
                duration += minutesDifference(multiFlight.get((i-1)/2).getArrivalTime(), multiFlight.get((i+1)/2).getDepartureTime());
            }
        }
        return durationString(duration);
    }

    public static String totalDuration(Date date1, Date date2) {
        return durationString(minutesDifference(date1, date2));
    }

    public static int minutesDifference(Date date1, Date date2) {
        return millisecondsToMins(Math.abs(date2.getTime() - date1.getTime()));
    }

    public static List<FlightInfo> singleFlightInfos(List<Flights> singleFlights) {
        List<FlightInfo> flightInfos = new ArrayList<FlightInfo>();
        for (Flights singleFlight : singleFlights) {
            flightInfos.add(singleFlightInfo(singleFlight));
        }
        return flightInfos;
    }

    public static FlightInfo singleFlightInfo(Flights singleFlight) {

        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return new FlightInfo(
            singleFlight.getAirline().getAirlineName(),  //"American Airlines"
            singleFlight.getFlightNumber(),  //"AA1735"
            dfTime.format(singleFlight.getDepartureTime()),  //"9:50 AM"
            dfDate.format(singleFlight.getDepartureTime()),  //"Sun, Sep 24"
            singleFlight.getDepartureCode().getAirport(),  //"Atlanta"
            durationString(singleFlight.getDuration()),  //"22h 10min"
            dfTime.format(singleFlight.getArrivalTime()),  //"9:00 AM"
            dfDate.format(singleFlight.getArrivalTime()),  //"Mon, Sep 25"
            singleFlight.getDestinationCode().getAirport(),  //"Rio De Janeiro"
            singleFlight.getPlaneCode(),  //"A380"
            String.format("%s-%s", singleFlight.getDepartureCodeString(), singleFlight.getDestinationCodeString()),  //"ATL-MIA-GIG"
            String.format("img/%s.png", singleFlight.getAirlineCode()),  //"img/AA.png"
            "0 stops",  //"1 stop"
            formatter.format(singleFlight.getPrice()),  //"$2022.70"
            singleFlight.getId() + "");
    }
}
