package seng3150.group3;

import seng3150.group3.beans.Flights;
import seng3150.group3.beans.Tag;
import seng3150.group3.beans.Review;
import seng3150.group3.service.ReviewDAO;
import seng3150.group3.beans.Price;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

//class for determining recommendations
public class Recommend {

    private List<ScoredFlight> ScoredList;
    private double costScore;
    private double durScore;

    //constructor
    public Recommend() {
        ScoredList = new ArrayList<>();
        costScore = 100;
        durScore = 100;
    }

    //pre: Called by system when returning search data
    //Post: sorts returned flights by an recommendation algorithm
    private List<ScoredFlight> run( List<List<Flights>> SearchList) {
        ReviewDAO reviewDAO = new ReviewDAO();
        for (int i = 0; i < SearchList.size(); i++) {
            ScoredList.add(new ScoredFlight());
            ScoredList.get(i).setFlight(SearchList.get(i));
            /* We don't have the tags do anything yet
            for (int k = 0; k < SearchList.get(i).size(); k++) {
                List<Review> reviewList = reviewDAO.getFlightReviews(SearchList.get(i).get(k).getId());
                for (int j = 0; j < reviewList.size(); j++) {
                    List<Tag> tagList = reviewDAO.getReviewTags(reviewList.get(j).getReviewID());
                    ScoredList.get(i).addTags(tagList);// this needs fixing
                }
            }*/
            // Category userCat = /*UserDatabase.getUserCategory*/;
            // ScoredList.get(i).weighTags(userCat); placeholder for future implementation
            // ScoredList.get(i).setScore();
        }

        Collections.sort(ScoredList, new Comparator<ScoredFlight>() {
            @Override
            public int compare(ScoredFlight o1, ScoredFlight o2) {
                return o1.compareTo(o2.getFlight());
            }
        });
        double flatScore = 0;
        // if (costPreference == true) {
        //  flatScore += costScore*9;
        // }
        flatScore += costScore;
        for (int i = 0; i < ScoredList.size(); i++) {

            ScoredList.get(i).addScore(flatScore);
            flatScore -= 5;
            if (flatScore == 0)
                break;
        }

        Collections.sort(ScoredList, new Comparator<ScoredFlight>() {
            @Override
            public int compare(ScoredFlight o1, ScoredFlight o2) {
                int totalDuration = 0;
                for (int i = 0; i < o2.getFlight().size(); i++) {
                    totalDuration += o2.getFlight().get(i).getDuration();
                }
                return o1.compareTo(totalDuration); //needs fixing
            }
        });
        flatScore = 0;
        /*if (durPreference == true) {
                flatScore += durScore*9;
            }*/
        flatScore += durScore;
        for (int i = 0; i < ScoredList.size(); i++) {
            ScoredList.get(i).addScore(flatScore);
            flatScore -= 5;
            if (flatScore == 0)
                break;
        }
        flatScore = 0;
        for (int i = 0; i < ScoredList.size(); i++) {
            int legs = ScoredList.get(i).getFlight().size();
            if (legs > 1) {
                flatScore = (legs*-50)+50;
            }
            ScoredList.get(i).addScore(flatScore);
        }

        Collections.sort(ScoredList, new Comparator<ScoredFlight>() {
            @Override
            public int compare(ScoredFlight o1, ScoredFlight o2) {
                return o1.compareTo(o2);
            }
        });
        return ScoredList;
    }

    //Pre: When system wants the sorted list but has called the method before
    //Post: returns a pre-sorted flight list
    public List<ScoredFlight> getRecommend(List<List<Flights>> SearchList) {
        if (ScoredList.size() == 0) {
            //in case the system hasn't actually done a sort yet
            run(SearchList);
        }
        return ScoredList;
    }

    public void clearRecommend() {
        ScoredList = new ArrayList<>();
        costScore = 100;
        durScore = 100;
    }

    public class ScoredFlight implements Comparable<ScoredFlight> {
        List<Flights> flight;
        List<ScoredTag> TagList;
        double score;

        ScoredFlight() {
            List<ScoredTag> TagList = new ArrayList<ScoredTag>();
            score = 0;
        }

        @Override
        public int compareTo(ScoredFlight o) {
            if(this.score > o.getScore()) {
                return 1;
            } else if(this.score == o.getScore()) {
                return 0;
            } else {
                return -1;
            }
        }

        public int compareTo(int o) {
            int totalDuration = 0;
            for (int i = 0; i < flight.size(); i++) {
                totalDuration += flight.get(i).getDuration();
            }
            if(totalDuration > o) {
                return 1;
            } else if(totalDuration == o) {
                return 0;
            } else {
                return -1;
            }
        }

        public int compareTo(List<Flights> o) {
            int priceA = 0;
            int priceB = 0;
            for (int i = 0; i < flight.size(); i++) {
                priceA += flight.get(i).getPrice();
            }
            for (int i = 0; i < o.size(); i++) {
                priceB += o.get(i).getPrice();
            }
            if(priceA > priceB) {
                return 1;
            } else if(priceA == priceB) {
                return 0;
            } else {
                return -1;
            }
        }

        public void setFlight(List<Flights> in) {
            flight = in;
        }

        public List<Flights> getFlight() {
            return flight;
        }

        private void addTags(List<Tag> inList) {
            for (int i = 0; i < inList.size(); i++) {
                boolean flag = false;
                for (int j = 0; j < TagList.size(); j++) {
                    if (inList.get(i).getTagName().equals(TagList.get(j).getTagName())) {
                        TagList.get(i).addNum(inList.get(i).getTagScore());
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    ScoredTag inTag = new ScoredTag(inList.get(i).getTagName(), inList.get(i).getTagScore());
                    TagList.add(i, inTag);
                }
            }
        }

        /*public void weighTags(Category input) {    placeholder for future implementation
            for (int i = 0; i < input.getWeighScores().size(); i++) {
                for (int j = 0; j < TagList.size(); j++) {
                    if (input.getWeighScores(i).getName() == TagList.get(j).getName) {
                        TagList.get(j).multi(input.getWeighScores(i).getWeight());
                    }
                }
        }*/

        private void addScore(double in) {
                score += in;

        }

        private void setScore() {
            for (int i = 0; i < TagList.size(); i++) {
                score += TagList.get(i).getTagScore();
            }
        }

        private double getScore() {
            return score;
        }
    }

    public class ScoredTag {
        String tagName;
        double tagScore;

        ScoredTag(String in, double score) {
            tagName = in;
            tagScore = score;
        }

        private void addNum(double input) {
            tagScore += input;
        }

        private void multi(double input) {
            tagScore = tagScore*input;
        }

        private String getTagName() {
            return tagName;
        }

        private double getTagScore() {
            return tagScore;
        }
    }
}
