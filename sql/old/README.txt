
In order to build new database:
1) Run 1UserSetup.sql
2) Run 2UserSQL.sql
3) Run 3Flights.sql
4) Recommended: In MySQL Workbench extend the timeout before running this as
   these actions may take over 30 seconds
     a) Go to Edit > Preferences > SQL Editor
     b) Change 'DBMS connection read timeout interval (in seconds)'
        to something much higher
     c) Close MySQL Workbench and reopen it
5) Run 4DatabaseRestructure.sql
6) Run 5FlightPaths.sql
7) Run 6Booking.sql
8) Run 7UserBooking.sql
9) Run 8Reviews.sql

NOTE:
  If the connection times out during step 5 it means you need to do step 4.
  When continuing, 4DatabaseRestructure.sql will get stuck at the RENAME
  command - just select everything below it.
