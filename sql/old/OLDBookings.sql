DROP TABLE IF EXISTS Booking;

CREATE TABLE Booking(
	bookingID int NOT NULL /*AUTO_INCREMENT*/,
	userID int NOT NULL,
	flightID int NOT NULL
	/*,PRIMARY KEY (bookingID)*/
	/*,
	FOREIGN KEY (userID) REFERENCES `USER` (userID),
	FOREIGN KEY (flightID) REFERENCES Flights (id)
	*/
)ENGINE=InnoDB DEFAULT CHARSET=latin1;
