DROP TABLE IF EXISTS Review;
DROP TABLE IF EXISTS Tags;
DROP TABLE IF EXISTS TagNames;

CREATE TABLE `Review` (
  `ReviewID` int NOT NULL auto_increment,
  `RFlight` int NOT NULL,
  `RUser` int NOT NULL,
  `RComment` varchar(200) NOT NULL,
  PRIMARY KEY (`ReviewID`),
  KEY `RFlight_FK` (`RFlight`),
  KEY `RUser_FK` (`RUser`),
  CONSTRAINT `RFlight_FK` FOREIGN KEY (`RFlight`) REFERENCES `Flights` (`id`),
  CONSTRAINT `RUser_FK` FOREIGN KEY (`RUser`) REFERENCES `User` (`userID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `TagNames` (
  `TagName` varchar(20) NOT NULL,
  PRIMARY KEY (`TagName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Tags` (
  `TagID` int NOT NULL auto_increment,
  `ReviewID` int NOT NULL,
  `TagName` varchar(20) NOT NULL,
  `TagScore` int NOT NULL,
  PRIMARY KEY (`TagID`),
  KEY `Review_FK` (`ReviewID`),
  KEY `TagName_FK` (`TagName`),
  CONSTRAINT `Review_FK` FOREIGN KEY (`ReviewID`) REFERENCES `Review` (`ReviewID`),
  CONSTRAINT `TagName_FK` FOREIGN KEY (`TagName`) REFERENCES `TagNames` (`TagName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
