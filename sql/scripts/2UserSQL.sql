#Creates the user database and grants privileges to user

CREATE DATABASE IF NOT EXISTS seng3050protouser;

USE seng3050protouser;

#creating table
set foreign_key_checks = 0;
DROP TABLE IF EXISTS `USER`;
DROP TABLE IF EXISTS `Role`;
DROP TABLE IF EXISTS UsersRoles;
set foreign_key_checks = 1;

create table `USER` (userID int primary key auto_increment,
	firstName VARCHAR(20) not null,
	lastName VARCHAR(20) not null,
	email VARCHAR(100) not null unique,
	phone VARCHAR(16),
	password VARCHAR(255) not null)ENGINE=InnoDB DEFAULT CHARSET=latin1;

#inserting a customer

insert into `USER` values (1,'Robert', 'Drop Tables', 'bobbytables@gmail.com', '84384933', 'hunter2');
insert into `USER` values (2, 'Rock', 'Thunderhard', 'r0ck@hotmail.com', '13213196', 'kangaroo');
insert into `USER` values (3, 'Admin','Person', 'admin@flightpub.com', '001', 'password');

CREATE TABLE `Role`(
	roleID int PRIMARY KEY auto_increment,
	roleName VARCHAR(20) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `role`(roleName) values ('Admin');
insert into `role`(roleName) values ('User');

CREATE TABLE UsersRoles(
	userID int NOT NULL,
	roleID int NOT NULL,
	PRIMARY KEY (userID, roleID),
	FOREIGN KEY (userID) REFERENCES `USER` (userID),
	FOREIGN KEY (roleID) REFERENCES Role (roleID)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into usersroles(userID,roleID) values(1,2);
insert into usersroles(userID,roleID) values(2, 2);
insert into usersroles(userID,roleID) values(3, 1);
