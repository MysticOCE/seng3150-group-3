DROP TABLE IF EXISTS UserBookings;

CREATE TABLE UserBookings(
	userID int NOT NULL,
	bookingID int NOT NULL,
	FOREIGN KEY (userID) REFERENCES `USER` (userID),
	FOREIGN KEY (bookingID) REFERENCES Booking (bookingID)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;
