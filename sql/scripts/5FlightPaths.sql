DROP TABLE IF EXISTS FlightPaths;  -- To fight bad juju. Grants 1000 years good luck
DROP VIEW IF EXISTS FlightPaths;
CREATE VIEW FlightPaths AS
(
	SELECT DepartureCode, DestinationCode, Duration
    FROM Flights
    group by DepartureCode, DestinationCode, Duration
);
