USE seng3050protouser;

/* Must drop the BookingOrder table before Booking as BookingOrder references booking table.*/
DROP TABLE IF EXISTS PurchaseHistory;
DROP TABLE IF EXISTS BookingOrder;
DROP TABLE IF EXISTS Booking;

CREATE TABLE Booking(
	bookingID int NOT NULL AUTO_INCREMENT,
	flightID int NOT NULL,
	ClassCode varchar(20) NOT NULL,
	PRIMARY KEY (bookingID),
	FOREIGN KEY (flightID) REFERENCES Flights (id),
	FOREIGN KEY (ClassCode) REFERENCES TicketClass (ClassCode)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE BookingOrder(
	bookingOrderID int NOT NULL AUTO_INCREMENT,
	userID int NOT NULL,
	bookingID int NOT NULL,
	PRIMARY KEY (bookingOrderID),
	FOREIGN KEY (userID) REFERENCES `USER` (userID),
	FOREIGN KEY (bookingID) REFERENCES Booking (bookingID)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE PurchaseHistory(
	purchaseHistoryID int NOT NULL AUTO_INCREMENT,
	userID int NOT NULL,
	bookingID int NOT NULL,
	inputTime datetime NOT NULL,
	PRIMARY KEY (purchaseHistoryID),
	FOREIGN KEY (userID) REFERENCES `USER` (userID),
	FOREIGN KEY (bookingID) REFERENCES Booking (bookingID)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;
