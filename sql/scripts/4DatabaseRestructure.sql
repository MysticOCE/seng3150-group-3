USE seng3050protouser;
RENAME TABLE Flights TO flights_old;
DROP TABLE IF EXISTS Flights;
CREATE TABLE `Flights` (
  `id` INT NOT NULL AUTO_INCREMENT,  -- Surrogate key to maintain ActiveJDBC compatibility
  `AirlineCode` char(2) NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `DepartureCode` char(3) NOT NULL,
  `DestinationCode` char(3) NOT NULL,
  `DepartureTime` datetime NOT NULL,
  `ArrivalTime` datetime NOT NULL,
  `PlaneCode` varchar(20) NOT NULL,
  `Duration` int(11) NOT NULL,
  PRIMARY KEY (`AirlineCode`,`FlightNumber`,`DepartureTime`),
  KEY `FlightsDepartureCode_FK` (`DepartureCode`),
  KEY `FlightsDestinationCode_FK` (`DestinationCode`),
  KEY `FlightsPlaneCode_FK` (`PlaneCode`),
  CONSTRAINT `FlightsPlaneCode_FK_new` FOREIGN KEY (`PlaneCode`) REFERENCES `PlaneType` (`PlaneCode`),
  CONSTRAINT `FlightsAirlineCode_FK_new` FOREIGN KEY (`AirlineCode`) REFERENCES `Airlines` (`AirlineCode`),
  CONSTRAINT `FlightsDepartureCode_FK_new` FOREIGN KEY (`DepartureCode`) REFERENCES `Destinations` (`DestinationCode`),
  CONSTRAINT `FlightsDestinationCode_FK_new` FOREIGN KEY (`DestinationCode`) REFERENCES `Destinations` (`DestinationCode`),
  UNIQUE INDEX (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Flights` (`AirlineCode`, `FlightNumber`, `DepartureCode`,`DestinationCode`, `DepartureTime`, `ArrivalTime`, `PlaneCode`, `Duration`)
SELECT `AirlineCode`, `FlightNumber`, `DepartureCode`,`DestinationCode`, `DepartureTime`, `ArrivalTime`, `PlaneCode`, `Duration`
FROM (
	/*No-leg flights*/
	SELECT AirlineCode, FlightNumber, DepartureCode, DestinationCode, DepartureTime, ArrivalTime, PlaneCode, Duration
	FROM flights_old
	WHERE StopOverCode IS NULL
	UNION
	/*First leg of multi-leg flights*/
	SELECT AirlineCode, FlightNumber, DepartureCode, StopOverCode AS DestinationCode, DepartureTime, ArrivalTimeStopOver AS ArrivalTime, PlaneCode, Duration
	FROM flights_old
	WHERE StopOverCode IS NOT NULL
	UNION
	/*Second leg of multi-leg flights*/
	SELECT AirlineCode, FlightNumber, StopOverCode AS DepartureCode, DestinationCode, DepartureTimeStopOver AS DepartureTime, ArrivalTime, PlaneCode, DurationSecondLeg AS Duration
	FROM flights_old
	WHERE StopOverCode IS NOT NULL ) AS Flight_Query;

UPDATE flights_old
SET ArrivalTimeStopOver = '2018-03-02 07:50:00', DepartureTimeStopOver = '2018-03-02 11:00:00', ArrivalTime = '2018-03-02 12:30:00'
WHERE  id = 9302;-- Malformed data, creates error in Availability table.alter

RENAME TABLE Availability to availability_old;
DROP TABLE IF EXISTS Availability;
CREATE TABLE `Availability` (
	`AirlineCode` char(2) NOT NULL,
	`FlightNumber` varchar(6) NOT NULL,
	`DepartureTime` datetime NOT NULL,
	`ClassCode` char(3) NOT NULL,
	`TicketCode` char(1) NOT NULL,
	`NumberAvailableSeats` int(11) NOT NULL,
	PRIMARY KEY (`AirlineCode`,`FlightNumber`,`DepartureTime`,`ClassCode`,`TicketCode`),
	KEY `AvailabilityClassCode_FK_new` (`ClassCode`),
	KEY `AvailabilityTicketCode_FK_new` (`TicketCode`),
	CONSTRAINT `AvailabilityTicketCode_FK_new` FOREIGN KEY (`TicketCode`) REFERENCES `TicketType` (`TicketCode`),
	CONSTRAINT `AvailabilityAirlineCode_FK_new` FOREIGN KEY (`AirlineCode`) REFERENCES `Airlines` (`AirlineCode`),
	CONSTRAINT `AvailabilityClassCode_FK_new` FOREIGN KEY (`ClassCode`) REFERENCES `TicketClass` (`ClassCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO Availability (`AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats`)
SELECT `AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats` FROM (
	SELECT a.AirlineCode, a.FlightNumber, a.DepartureTime, a.ClassCode, a.TicketCode, a.NumberAvailableSeatsLeg1 AS NumberAvailableSeats
	FROM availability_old a LEFT JOIN flights_old f ON
		a.AirlineCode = f.AirlineCode AND
		a.FlightNumber = f.FlightNumber AND
		a.DepartureTime = f.departureTime
	WHERE a.NumberAvailableSeatsLeg2 IS NOT NULL) AS Availability_Query1;

INSERT INTO Availability (`AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats`)
SELECT `AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats` FROM (
	SELECT a.AirlineCode, a.FlightNumber, f.DepartureTimeStopOver AS DepartureTime, a.ClassCode, a.TicketCode, a.NumberAvailableSeatsLeg2 AS NumberAvailableSeats
	FROM availability_old a LEFT JOIN flights_old f ON
		a.AirlineCode = f.AirlineCode AND
		a.FlightNumber = f.FlightNumber AND
		a.DepartureTime = f.departureTime
	WHERE a.NumberAvailableSeatsLeg2 IS NOT NULL) AS Availability_Query2;

INSERT INTO Availability (`AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats`)
SELECT `AirlineCode`, `FlightNumber`, `DepartureTime`, `ClassCode`, `TicketCode`, `NumberAvailableSeats` FROM (
	SELECT a.AirlineCode, a.FlightNumber, a.DepartureTime, a.ClassCode, a.TicketCode, a.NumberAvailableSeatsLeg1 AS NumberAvailableSeats
	FROM availability_old a
	WHERE a.NumberAvailableSeatsLeg2 IS NULL ) AS Availability_Query3;
