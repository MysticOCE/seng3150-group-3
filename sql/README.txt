############################################
Steps to run this script:
############################################

  1. Open up a command window at this folder location (in the 'sql' folder)

  2. Run this command: 
  mysql -u root -p
     - This will ask for your root password: enter it

  3. Once connected to mysql, run this command:
  \. build_db.txt

  4. Type exit to exit

Please note that this cannot be run in MySQL Workbench as it isn't SQL.

############################################
