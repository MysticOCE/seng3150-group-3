**Note:** If running the project in IntelliJ, the URL will be `http://localhost:8080/`
and will need to be changed in Utility.getTestRoot

# Running the webapp #
1. **Build database**
    a) Follow instructions in `sql\README.txt`

2. **Assemble and deploy .war**
    a) Run `./gradlew assemble` in project root directory

    b) Move the `build/libs/FlightPub-0.2.war` to yout Tomcat `webapps/` directory

    c) Start Tomcat if you haven't already, and navigate to
     `http://localhost:8080/FlightPub-0.2/` in your browser

# Testing the webapp #
After following 'RUNNING THE WEBAPP' above:
3. **Run the test cases**
    a) Run `./gradlew test` in project root directory
